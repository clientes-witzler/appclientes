<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="row" id="updateContratos" style="margin: 0;">
    <?php
    $contarChart = 1;
    //include "../../backend/contratos/contratos.php";

    if (empty($armazenaNome)) {
    } else {
        //echo "<script>console.log('".count($arrId_distribuidora)."')</script>";
        //Tratamento de dados de contratos2:
        $totalContratos2 = count($arrId);
        /*for($i = 0; $i < $totalContratos2; $i++){

        }*/

        $tamanhoContratos = count($armazenaNome);
    ?>
        <!-- ANIMACAO JAVASCRIPT AO CARREGAR CONTRATOS -->
        <script>
            $("#updateContratos").html("<div id='loadContratos' class='col-12 d-flex align-items-center text-center justify-content-center' style='height: 550px; background: white;'><span class='loader'></span></div>");
            setTimeout(function() {
                document.getElementById('loadContratos').remove();
                for (var i = 0; i < <?php echo $tamanhoContratos; ?>; i++) {
                    document.getElementById('contrato' + i).style.display = 'block';
                }
            }, 2500);
        </script>
        <?php
        for ($i = 0; $i < $tamanhoContratos; $i++) {
        ?>
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 fade-in" id="contrato<?php echo $i; ?>" style="display: none;">
                <div class="card-accent-secondary card">
                    <div class="text-center h-25 card-header">
                        <div class="row">
                            <div class="col">
                                <div class="mb-0 pt-3 card-title">
                                    <img height="50" src="<?php echo $armazenaPath[$i]; ?>">
                                    <hr>
                                    <h5 class="mt-3"><?php echo strtoupper($armazenaInicio_suprimento[$i]) . " A " . strtoupper($armazenaFim_suprimento[$i]); ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-6 noPadding text-left">
                                                    Comercializadora:
                                                </div>
                                                <div class="col-6 noPadding text-right">
                                                    <?php echo $armazenaNome[$i]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-6 noPadding text-left">
                                                    Número do contrato:
                                                </div>
                                                <div class="col-6 noPadding text-right">
                                                    <?php echo $armazenaNumero_contrato[$i]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-6 noPadding text-left">
                                                    Preço Contratado [R$/MWh]:
                                                </div>
                                                <div class="col-6 noPadding text-right">
                                                    <?php echo $armazenaPreco_base[$i]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-6 noPadding text-left">
                                                    Flexibilidade:
                                                </div>
                                                <div class="col-6 noPadding text-right">
                                                    <?php echo $armazenaFlex_inf[$i] . "/" . $armazenaFlex_sup[$i]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-6 noPadding text-left">
                                                    Sazonalidade:
                                                </div>
                                                <div class="col-6 noPadding text-right">
                                                    <?php echo $armazenaSazo_inf[$i] . "/" . $armazenaSazo_sup[$i]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="height: 250px; ">
                                                <canvas id="chartDoughnutContrato<?php echo $contarChart; ?>" height="100" width="100" class="mx-auto" style="display: block;"></canvas>
                                                <div class="chartWrapper" style="position: relative;">
                                                    <div class="chartAreaWrapper" id="graficoContrato<?php echo $contarChart; ?>" style="overflow-x: auto;">
                                                    </div>
                                                </div>
                                                <script>
                                                    var barChartContrato<?php echo $contarChart; ?> = {
                                                        labels: labelsChart,
                                                        datasets: [{
                                                                label: 'Flex máx',
                                                                data: [
                                                                    20,
                                                                    18,
                                                                    20,
                                                                    18,
                                                                    20,
                                                                    18,
                                                                    20,
                                                                    18,
                                                                    20,
                                                                    18,
                                                                    20,
                                                                    18
                                                                ],
                                                                type: "line",
                                                                fill: false,
                                                                backgroundColor: "rgba(76, 142, 173, 0.7)",
                                                                borderColor: "rgba(76, 142, 173, 0.7)",
                                                                borderWidth: 2,
                                                                lineTension: 0
                                                            },
                                                            {
                                                                label: 'Valor médio',
                                                                data: [
                                                                    17,
                                                                    15,
                                                                    17,
                                                                    15,
                                                                    17,
                                                                    15,
                                                                    17,
                                                                    15,
                                                                    17,
                                                                    15,
                                                                    17,
                                                                    15
                                                                ],
                                                                type: "line",
                                                                fill: false,
                                                                backgroundColor: "rgba(255, 140, 0, 0.7)",
                                                                borderColor: "rgba(255, 140, 0, 0.7)",
                                                                borderWidth: 2,
                                                                lineTension: 0
                                                            },
                                                            {
                                                                label: 'Flex min',
                                                                data: [
                                                                    14,
                                                                    12,
                                                                    14,
                                                                    12,
                                                                    14,
                                                                    12,
                                                                    14,
                                                                    12,
                                                                    14,
                                                                    12,
                                                                    14,
                                                                    12
                                                                ],
                                                                type: "line",
                                                                fill: false,
                                                                backgroundColor: "rgba(176, 176, 176, 0.7)",
                                                                borderColor: "rgba(176, 176, 176, 0.7)",
                                                                borderWidth: 2,
                                                                lineTension: 0
                                                            }
                                                        ]
                                                    };

                                                    var ctxContrato<?php echo $contarChart; ?> = document.getElementById('chartDoughnutContrato<?php echo $contarChart; ?>').getContext('2d');
                                                    window.myBar = new Chart(ctxContrato<?php echo $contarChart; ?>, {
                                                        type: 'line',
                                                        data: barChartContrato<?php echo $contarChart; ?>,
                                                        options: {
                                                            responsive: true,
                                                            maintainAspectRatio: false,
                                                            legend: {
                                                                position: 'top',
                                                            },
                                                            title: {
                                                                display: false,
                                                                text: 'Comercializadoras'
                                                            },
                                                            animation: {
                                                                animateScale: true,
                                                                animateRotate: true,
                                                                duration: 2000,
                                                            },
                                                            scales: {
                                                                yAxes: [{
                                                                    ticks: {
                                                                        min: 0,
                                                                    },
                                                                }, ]
                                                            }
                                                        }
                                                    });
                                                </script>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    <?php
            $contarChart++;
        }
    }
    ?>
</div>