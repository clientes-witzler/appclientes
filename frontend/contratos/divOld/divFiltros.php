<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="text-value card-header">
   <div class="row">
      <div class="col">
         <div class="mb-0 card-title">Filtros</div>
      </div>
   </div>
</div>
<div class="card-body">
   <div class="row d-flex">
      <div class="col-lg-4 col-md-3 col-sm-2"></div>
      <div class="d-flex centerInMobile text-center justify-content-center col-sm-8 col-lg-4 col-md-6 col-xs-12">
         <div class="dropdown2 dropdown input-group-prepend no-marginright">
            <label class="text-value input-group-text">Unidade: </label>
         </div>
         <!-- Aqui carrega o arquivo que tem as opcoes de unidade -->
         <?php include "../../backend/dadosUnidades/dadosUnidades.php"; ?>
      </div>
      <div class="col-lg-4 col-md-3 col-sm-2"></div>
   </div>
</div>