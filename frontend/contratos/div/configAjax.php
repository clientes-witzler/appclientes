<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    var checkCookie = function() {
        var lastCookie = document.cookie;
        //Aqui separamos as id de unidade para cada uma das buscas:
        var last_id_unidades_contratos = $("#idUnidades").find(':selected').attr('id');
        var last_calendario_unidade_contratos = $("#datasContratos").val();
        return function() {
            var currentCookie = document.cookie;

            //Codigo para mudanca de unidade contrato:
            var current_id_unidades_contratos = $("#idUnidades").find(':selected').attr('id');
            if (current_id_unidades_contratos != last_id_unidades_contratos) {
                last_id_unidades_contratos = current_id_unidades_contratos;
                //Editando o grafico consumo por dia
                //Remover div:
                $("#updateContratos").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/contratos/updateCardContratos.php',
                    data: {
                        'id_unidades': last_id_unidades_contratos,
                        'ano': last_calendario_unidade_contratos,
                    },
                    beforeSend: function(carrega) {
                        $("#updateContratos").html("<div id='loadContratos' class='col-12 d-flex align-items-center text-center justify-content-center' style='height: 550px; background: white;'><span class='loader'></span></div>");
                    },
                    success: function(r) {
                        setTimeout(function() {
                            $("#updateContratos").html(r);
                        }, 2500);
                    }
                });

                $("#graficoComercializadoras").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/contratos/updateComercializadoras.php',
                    data: {
                        'id_unidades': last_id_unidades_contratos,
                        'ano': last_calendario_unidade_contratos,
                        'tipo': 1
                    },
                    success: function(s) {
                        $("#graficoComercializadoras").html(s);
                    }
                });

                $("#graficoMontantes").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/contratos/updateMontantes.php',
                    data: {
                        'id_unidades': last_id_unidades_contratos,
                        'ano': last_calendario_unidade_contratos,
                        'tipo': 2
                    },
                    success: function(s) {
                        $("#graficoMontantes").html(s);
                    }
                });
            }

            //Codigo pra mudanca de data contrato
            var current_calendario_contratos = $("#datasContratos").val();
            if (current_calendario_contratos != last_calendario_unidade_contratos) {
                last_calendario_unidade_contratos = current_calendario_contratos;
                console.log("Novo valor de calendario: " + last_calendario_unidade_contratos);
                //editando os graficos:
                $("#updateContratos").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/contratos/updateCardContratos.php',
                    data: {
                        'id_unidades': last_id_unidades_contratos,
                        'ano': last_calendario_unidade_contratos,
                    },
                    beforeSend: function(carrega) {
                        $("#updateContratos").html("<div id='loadContratos' class='col-12 d-flex align-items-center text-center justify-content-center' style='height: 550px; background: white;'><span class='loader'></span></div>");
                    },
                    success: function(r) {
                        setTimeout(function() {
                            $("#updateContratos").html(r);
                        }, 2500);
                        //console.log(r);
                    }
                });

                $("#graficoComercializadoras").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/contratos/updateComercializadoras.php',
                    data: {
                        'id_unidades': last_id_unidades_contratos,
                        'ano': last_calendario_unidade_contratos,
                        'tipo': 1
                    },
                    success: function(s) {
                        $("#graficoComercializadoras").html(s);
                    }
                });

                $("#graficoMontantes").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/contratos/updateMontantes.php',
                    data: {
                        'id_unidades': last_id_unidades_contratos,
                        'ano': last_calendario_unidade_contratos,
                        'tipo': 2
                    },
                    success: function(s) {
                        $("#graficoMontantes").html(s);
                    }
                });
            }
        };
    }();
    window.setInterval(checkCookie, 100);
</script>