<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="row" id="updateDadosUnidades">
    <?php

    include "../../backend/unidades/unidades.php";
    if (empty($arrayNomeUnidade)) {
    } else {
        $tamanhoNomeUnidades = count($arrayNomeUnidade);
        for ($i = 0; $i < $tamanhoNomeUnidades; $i++) {

    ?>
            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                <div class="card-accent-secondary card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="pt-2 col-2 text-center">
                                <!--i class="c-icon c-icon-2xl cil-truck"></i-->
                                <img src="../../backend/assets/img/unidades/INDÚSTRIA_32 px BLUE.svg">
                            </div>
                            <div class="pt-3 col-10">
                                <div class="card-title">
                                    <h5><b><?php echo $arrayNomeUnidade[$i]; ?></b></h5>
                                    <b>UC: </b><?php echo $arrayUcUnidade[$i]; ?>
                                    <br />
                                    <b>Medidor: </b><?php echo $arrayMedidorUnidade[$i]; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" id="containerUnidade<?php echo $i; ?>" style="height: 235px; padding-top: 10px;">
                        <script>
                            $("#containerUnidade<?php echo $i; ?>").html("<div id='loadUnidade<?php echo $i; ?>' class='col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                            setTimeout(function() {
                                document.getElementById('loadUnidade<?php echo $i; ?>').remove();
                                document.getElementById('conteudoUnidade<?php echo $i; ?>').style.display = 'block';
                            }, 2500);
                        </script>

                        <div class="table-responsive fade-in" style="display: none;" id="conteudoUnidade<?php echo $i; ?>">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td style="border-top: 0;">
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-6 noPadding text-left">
                                                    <b>Distribuidora:</b>
                                                </div>
                                                <div class="col-6 noPadding text-center">
                                                    <?php echo $arrayNomeDistribuidora[$i]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-4 noPadding text-left"></div>
                                                <div class="col-4 noPadding text-center">
                                                    <b>ÚLTIMO</b>
                                                </div>
                                                <div class="col-4 noPadding text-center">
                                                    <b>ATUAL</b>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: 0;">
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-4 noPadding text-left">
                                                    <b>Consumo:</b>
                                                </div>
                                                <div class="col-4 noPadding text-center">
                                                    <?php echo $arrayTotalAcumuladoMensalAnterior[$i]; ?>
                                                </div>
                                                <div class="col-4 noPadding text-center">
                                                    <?php echo $arrayTotalAcumuladoMensalAtual[$i]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> 
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-4 noPadding text-left">
                                                    <b>Demanda:</b>
                                                </div>
                                                <div class="col-4 noPadding text-center">
                                                    <?php echo $arrayDemandaForaPonta[$i][1]; ?><br />
                                                    <?php echo $arrayDemandaPonta[$i][1]; ?>
                                                </div>
                                                <div class="col-4 noPadding text-center">
                                                    <?php echo $arrayDemandaForaPonta[$i][0]; ?><br />
                                                    <?php echo $arrayDemandaPonta[$i][0]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!--<div class="table-responsive fade-in" id="conteudoUnidade<?php echo $i; ?>" style="display: none;">
                     <table class="table" style="margin-top: -10px">
                        <thead>
                           <tr>
                              <th style=" border-top:0px;"></th>
                              <th class="text-center" style="border-top:0px;">ULTIMO</th>
                              <th class="text-center" style="border-top:0px;">|</th>
                              <th class="text-center" style="border-top:0px;">ATUAL</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <th style=""><b>Consumo:</b></th>
                              <td class="text-center"><?php echo $arrayTotalAcumuladoMensalAnterior[$i]; ?></td>
                              <td class="text-center" style="">|</td>
                              <td class="text-center"><?php echo $arrayTotalAcumuladoMensalAtual[$i]; ?></td>
                           </tr>
                           <tr>
                              <th style=""><b>Demanda:</b></th>
                              <td class="text-center"><?php echo $arrayDemandaForaPonta[$i][1]; ?> <?php echo $arrayDemandaPonta[$i][1]; ?></td>
                              <td class="text-center" style="">|</td>
                              <td class="text-center"><?php echo $arrayDemandaForaPonta[$i][0]; ?> <?php echo $arrayDemandaPonta[$i][0]; ?></td>
                           </tr>
                           <tr>
                              <th style=""><b>UC:</b></th>
                              <td class="text-center"></td>
                              <td class="text-center" style=""><?php echo $arrayUcUnidade[$i]; ?></td>
                              <td class="text-center"></td>
                           </tr>
                           <tr>
                              <th style=""><b>Distribuidora:</b></th>
                              <td class="text-center"></td>
                              <td class="text-center"><?php echo $arrayNomeDistribuidora[$i]; ?></td>
                              <td class="text-center"></td>
                           </tr>
                           <tr>
                              <th style=""><b>Medidor:</b></th>
                              <td class="text-center"></td>
                              <td class="text-center" style=""><?php echo $arrayMedidorUnidade[$i]; ?></td>
                              <td class="text-center"></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>-->

                    </div>
                </div>
            </div>




    <?php
        }
    }
    ?>

</div>