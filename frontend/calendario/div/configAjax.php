<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    var checkCookie = function() {
        var lastCookie = document.cookie;
        //Aqui separamos as id de unidade para cada uma das buscas:
        var last_id_unidades_calendario = $("#idUnidades").find(':selected').attr('id');
        var last_calendario_calendario = $("#datasCalendario").val();
        return function() {
            var currentCookie = document.cookie;

            //Codigo para mudanca de grafico ao mudar de mes ou palavra chave:
            var current_id_unidades_calendario = $("#idUnidades").find(':selected').attr('id');
            var current_calendario_calendario = "01-"+$("#datasCalendario").val();
            if(current_id_unidades_calendario != last_id_unidades_calendario || current_calendario_calendario != last_calendario_calendario){
                last_id_unidades_calendario = current_id_unidades_calendario;
                last_calendario_calendario = current_calendario_calendario;
                /*var anoNovo = last_calendario_calendario.substr(0, 4);
                var mesNovo = last_calendario_calendario.substr(5, 2);*/
                var anoNovo = last_calendario_calendario.substr(6, 10);
                var mesNovo = last_calendario_calendario.substr(3, 2);
                var finalNovo = new Date(anoNovo, mesNovo, 0);
                var ultimoDia = finalNovo.getDate();
                var dataFinalFormatada = anoNovo+'-'+mesNovo+'-'+ultimoDia;
                //Editando o grafico consumo por dia
                //Remover div:
                $("#calendar-body").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/calendario/updateCalendario.php',
                    data: {
                        'id_unidades': last_id_unidades_calendario,
                        'data_calendario': last_calendario_calendario,
                        'final_mes_calendario': dataFinalFormatada,
                        'ano_novo_calendario': anoNovo,
                        'mes_novo_calendario': mesNovo,
                        'ultimo_dia_calendario': ultimoDia

                    },
                    success: function(r){
                        //setTimeout(function(){
                            $("#calendar-body").html(r);
                            //console.log(r);
                        //}, 4000);
                    }
                });
            }
        };
    }();
    window.setInterval(checkCookie, 100);

    
</script>