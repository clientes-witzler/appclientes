<?php
  $error = "";
  header('Content-type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<html lang="pt-BR">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Portal dos clientes do Grupo Witzler">
    <meta name="author" content="Grupo Witzler">
    <meta name="keyword" content="Mercado livre de Energia, Witzler Energia, Grupo Witzler">
    <title>Witzler Energia | Mercado Livre de Energia</title>
    <link rel="apple-touch-icon" sizes="57x57" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="60x60" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="72x72" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="76x76" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="114x114" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="120x120" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="144x144" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="152x152" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="180x180" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="icon" type="image/png" sizes="192x192" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="icon" href="../bibliotecas/icones/FAVICON/FAVICON_WITZLER_ENERGIA.ico">
    <link rel="manifest" href="../bibliotecas/assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#0b1f50">
    <meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#0b1f50">
    <!-- Main styles for this application-->
    <link href="../bibliotecas/css/style.css" rel="stylesheet">
    <!-- CSS PARA TEMA GERAL -->
    <style>
      .btn.btn-link,a,a:hover{color:#000FFF}.btn.btn-link,a:focus{box-shadow:none}.btn.btn-primary{background-color:#0b1f50;color:#fff;border-color:rgb(4.8351648352, 13.6263736264, 35.1648351648)}.btn-primary.focus,.btn-primary:focus,.btn-primary:not(:disabled):not(.disabled):active,.btn-primary:not(:disabled):not(.disabled):active:focus{background-color:rgb(4.8351648352, 13.6263736264, 35.1648351648);border-color:rgb(4.8351648352, 13.6263736264, 35.1648351648);color:#fff;box-shadow:0 0 0 .2rem rgba(4.8351648352, 13.6263736264, 35.1648351648,0.5)}.btn.btn-secondary{background-color:#000FFF;color:#fff;border-color:#000CCC}.btn-secondary.focus,.btn-secondary:focus,.btn-secondary:not(:disabled):not(.disabled):active,.btn-secondary:not(:disabled):not(.disabled):active:focus{background-color:#000CCC;border-color:#000CCC;color:#fff;box-shadow:0 0 0 .2rem rgba(0,12,204,.5)}form input.form-control:focus{box-shadow:0 0 0 .2rem rgb(4.8351648352, 13.6263736264, 35.1648351648,.5);border-color:#0b1f50}
    </style>
    <meta name="robots" content="noindex">
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script>
  </head>
  <body class="c-app flex-row align-items-center" style="background-image: url(../bibliotecas/assets/img/background-3.9902c0af.jpg); background-repeat: no-repeat; background-attachment: fixed; background-position: center; ">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-5">
          <div class="card-group">
            <div class="card p-4" style="background-color: rgba(255, 255, 255, 0.8);">
              <div class="card-body">
                <!--<h1>Login</h1>
                <p class="text-muted">Entre na sua conta</p>-->
                  <div style="display: flex; justify-content: center; align-items: center; margin-top: -10px; padding-bottom: inherit; width:80%;">
                    <img src="../bibliotecas/assets/img/logo_grupo_witzler.svg" style="padding-left: 65px;">
                    
                  </div>
                  <p class="text-muted text-center pb-3">Esqueceu sua senha? Não se preocupe! <br> Cuidamos disso para você também! :)</p>
                  <form method="post" id="formulario-email" id="formulario-email" action="recupera.php">
                  
                  <div class="input-group mb-3" style="padding-left: 25px; padding-right: 40px;">
                       <div class="input-group-prepend"><span class="input-group-text"><img src="../bibliotecas/icones/ICONES_USUARIO.svg" height="16" width="16"></span></div>
                       <input placeholder="Digite seu usuário" autocomplete="username" id="username" name="username" type="text" class="form-control" />
                    </div>
                    
                    <div class="row">
                      <div class="col-12 text-right" style="padding-right: 55px; padding-left: 40px;">
                        <button style="width:100%;" class="btn btn-primary" type="submit" class="form-control" name="recupera" id="recupera" value="Enviar e-mail de recuperação">
                          <span class="fab fa-facebook-f"></span>
                          Enviar e-mail de recuperação
                        </button>
                      </div>              
                    </div>
                    <div class="row">
                      <div class="col-12" style="padding-left: 40px; padding-top: 10px;padding-right: 55px;">
                        <button style="width:100%;" class="btn btn-secondary" type="button" onclick="window.location.href='../login/index.php'">
                          Voltar para login
                        </button>
                      </div>
                    </div>
                </form>
                <?php
                  /*if(isset($_GET['erro']) && $_GET['erro'] == "ok"){
                    echo '<br/><div class="alert alert-primary alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Um email foi enviado aos administradores do sistema, por favor, espere até receber uma resposta.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                  }else if(isset($_GET['erro']) && $_GET['erro'] == "no-email"){
                    echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Nao foi inserido nenhum email.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                  }else if(isset($_GET['erro']) && $_GET['erro'] == "no-valid"){
                    echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">O email inserido nao é valido..<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                  }else if(isset($_GET['erro']) && $_GET['erro'] == "inexistente"){
                    echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Não encontramos um email para recuperação, por favor falar com a atendente!<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                  }*/
                  if(isset($_GET['msg'])){
                    $msg = strip_tags($_GET['msg']);
                    $msg = stripcslashes($msg);
                    switch($msg){
                      case "ok":
                        echo '<br/><div class="alert alert-primary alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">A nova senha foi enviada no seu email.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                      break;
                      default:
                        echo '';
                      break;
                    }
                  }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="../bibliotecas/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
    <!--[if IE]><!-->
    <script src="../bibliotecas/vendors/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->
  </body>
</html>