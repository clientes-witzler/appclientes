<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php

session_start();

$username = $_POST['username'];

// Sanitizando input email
$username = stripcslashes($username);
$username = strip_tags($username);

$conn_string = "host=192.168.11.201 port=5432 dbname=postgres user=wtzdb01-prd password=W1tzl3r3sc0web#db01-prd";
$conn = pg_connect($conn_string);

if(!$conn) {
    echo "ERROR";
    exit;
}

$result = pg_query($conn, "SELECT * FROM pc.web_cliente WHERE username='$username';");

if(!$result) {
    echo "ERROR in result!";
    exit;
}

$row = pg_fetch_assoc($result);
$emailResposta = $row['email'];

if(!$emailResposta || $emailRespota = NULL) {
    header("Location: index.php?erro=inexistente");
    exit();
    $error = '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Não encontramos um email para recuperação, por favor falar com o(a) atendente!<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
    echo $error;
} else {
    //echo "Existe!";
    $usernameI = 'recuperasenha';
    $u_password = '6w>~[uR46?v]v7DY';
    $url = $_SESSION['conexao']."/api/usuarios/auth";
    $curl = curl_init();
    //Cria requisição em json:
    $request = '{
        "u_password": "'.$u_password.'",
        "username": "'.$usernameI.'"
    }';
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json']);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    $err = curl_error($curl);

    $response = json_decode($result, true);
    if(isset($response['token'])){
        $token = $response['token'];    
        if($token == $response['token']){
            //echo "Token: ".$token;
            $_SESSION['token'] = $response['token'];
        }
        $result = pg_query($conn, "SELECT * FROM pc.web_login WHERE username='$username';");
        
        if(!$result) {
            echo "ERROR in result!";
            exit;
        }

        $row = pg_fetch_assoc($result);
        $id_login = $row['id_login'];

        $travaRecuperarSenha = "true";
        $montarPathsUrlLogin = "";
        $emailDividido = "";

        $emailDividido = explode("@",$emailResposta);

        if(strlen($emailDividido[0]) > 2 && (strpos($emailResposta,"@") !== false) && !(strpos($emailResposta,";") !== false)){ // e-mail válido
            $travaRecuperarSenha = "false";

            if(strlen($emailDividido[0]) > 2 && strlen($emailDividido[0]) < 5){
                $emailMontado = substr($emailDividido[0],0,1)."***".substr($emailDividido[0],-1)."@".$emailDividido[1];
                $montarPathsUrlLogin = "erro=senhaAlterada&emailEnviado=".$emailMontado;
            }else{
                $emailMontado = substr($emailDividido[0],0,2)."***".substr($emailDividido[0],-2)."@".$emailDividido[1];
                $montarPathsUrlLogin = "erro=senhaAlterada&emailEnviado=".$emailMontado;
            }

        }
        
        if($travaRecuperarSenha == "true"){ // BLOQUEADO POR ALGUM MOTIVO
            $montarPathsUrlLogin = "erro=recuperarSenha";
        }else{ // OK, ENVIANDO E-MAIL
            $novaSenha = md5("X2dftg4@s3!4d#sfgre3!Y@4B!x".$id_login.$username.$token); 

            $urlMudaSenha = $_SESSION['conexao']."/api/usuarios/update/".$id_login;

            $cabecalho = array("Authorization: Bearer ".$_SESSION['token'], "Content-Type: application/json");

            $data = array(
                "admin" => "false", 
                "id_login" => $id_login, 
                "u_password" => $novaSenha, 
                "username" => $username
            );

            $data = json_encode($data, true);

            $ch = curl_init($urlMudaSenha);

            // echo $_SESSION['token'];

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $cabecalho);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $response = curl_exec($ch);
            $err = curl_error($curl);

            if($err) {
                echo "error";
            }

            curl_close($ch);
            // var_dump($response);

            $resultInsercaoEmail = pg_query($conn, "INSERT INTO px.web_mail_sender(
                type_d, paths_body, paths_attach, m_to, cc, bcc, success,
                client_id, unit_id,date_ref, mail, pass, reports_data_id, collab_done,
                date_request, date_sent, ind_cons, mail_to_name, read_status)
            VALUES (
                9002, '$novaSenha', '', '$emailResposta', '', '', false,
                0, 0, now(), '', '', -1, -1, now(),
                now(), false, '', false
                );
            ");
        }
        
        session_unset();
        session_destroy();

        header("Location: ../login/index.php?".$montarPathsUrlLogin);
        exit();


    }

}

?>