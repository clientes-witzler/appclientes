<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
session_start();

if(isset($_POST['senha']) && isset($_POST['novaSenha']) && isset($_POST['confirmarSenha']) && isset($_SESSION['token'])){
    //Pega id e nome:
    $usuario = $_SESSION['usuario'];
    $senha = $_SESSION['senha'];
    $token = $_SESSION['token'];


    $conn_string = "host=192.168.11.201 port=5432 dbname=postgres user=wtzdb01-prd password=W1tzl3r3sc0web#db01-prd";
    $conn = pg_connect($conn_string);

    if(!$conn) {
        echo "ERROR";
        exit;
    }

    $result = pg_query($conn, "SELECT * FROM pc.web_login WHERE username='$usuario';");

    if(!$result) {
        echo "ERROR in result!";
        exit;
    }


    $row = pg_fetch_assoc($result);
    $user_id = $row['id_login'];

    //$user_id = $_SESSION['user_id'];
    //Declara variáveis
    $senhaAtual = $_POST['senha'];
    $novaSenha = $_POST['novaSenha'];
    $confirmarSenha = $_POST['confirmarSenha'];

        //Recolhe ID:
        $urlClientes = $_SESSION['conexao']."/api/clientes/id/cliente/".$usuario;
        $context = stream_context_create(array(
            'http' => array(
                'header' => "Authorization: Bearer ".$_SESSION['token'],
            ),
        ));
        @$recolheClientes = file_get_contents($urlClientes, false, $context);
        @$resultClientes = json_decode($recolheClientes);
        if(isset($resultClientes)){
            foreach($resultClientes as $dadosCliente){
                $id_login = $dadosCliente->id_login;
                $u_password = $dadosCliente->u_password;
            }
        }
        if($novaSenha == $confirmarSenha ){
            $urlMudaSenha = $_SESSION['conexao']."/api/usuarios/update/".$user_id;
            
            //echo "<script>console.log($user_id);</script>";

            $cabecalho = array("Authorization: Bearer ".$_SESSION['token'], "Content-Type: application/json");
            
            $data = array(
                "admin" => "false", 
                "id_login" => $user_id, 
                "u_password" => $novaSenha, 
                "username" => $usuario
            );
            
            $data = json_encode($data, true);

            //var_dump($data);

            $ch = curl_init($urlMudaSenha);
            //curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $cabecalho);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            //curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"admin\": false, \"id_login\": $user_id, \"u_password\": \"$novaSenha\", \"username\": \"$usuario\"}");
            $response = curl_exec($ch);
            $err = curl_error($ch);

            if($err) {
                echo "error";
            }
            
            curl_close($ch);
            //var_dump($response);
            header("Location: index.php?msg=ok");
            exit();
            
        }else{
            echo "<script>console.log('A nova senha nao é igual a confirmacao dela');</script>";
            header("Location: index.php?msg=senhadiferente");
            exit();
        }

}else{
    echo "error";
}
?>