<footer class="c-footer">
    <div>Grupo Witzler </a> © 2023 Equipe de desenvolvimento.</div>
</footer>

<!-- VERIFICADOR SE SESSION NÃO PASSOU DO TEMPO DE EXPIRAÇÃO -->
<script>
/*$(function(){
	setInterval(function(){	
        $.ajax({
            type: "POST",
            url: '../../backend/verifica_expirar.php',
            success: function(responses) {
                console.log(responses);
                if(responses == "expirou a sesssão"){
                    window.location.href = "../login/index.php?erro=sessaoexpirou";
                }
            },
        });
	}, 10000)
})*/
</script>

<!-- CoreUI and necessary plugins-->
<script src="../bibliotecas/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
<!--[if IE]><!-->
<script src="../bibliotecas/vendors/@coreui/icons/js/svgxuse.min.js"></script>
<!--<![endif]-->
<!-- Plugins and scripts required by this view-->
<script src="../bibliotecas/vendors/@coreui/chartjs/js/coreui-chartjs.bundle.js"></script>
<script src="../bibliotecas/vendors/@coreui/utils/js/coreui-utils.js"></script>
<script src="../bibliotecas/js/main.js"></script>
<script src="../bibliotecas/js/config.js"></script>
<script src="../bibliotecas/js/datas.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js " integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo " crossorigin="anonymous "></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js " integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6 " crossorigin="anonymous "></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.js'></script>
<?php
    
    if($_SESSION['type_client'] == 2){
        $url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        if(!strpos($url,'faq')){
            echo('
                <!-- Chat Movidesk -->
                <script type=\'text/javascript\'>var mdChatClient="A6FFB6AE6DD24CA391029F17FD1DA8E1";</script>
                <script src=\'https://chat.movidesk.com/Scripts/chat-widget.min.js\'></script>
                <!-- Chat do Movidesk fim -->
            ');
        }
    }

?>
</body>
</html>