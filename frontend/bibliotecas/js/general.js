function mudarUnidade(idSelect, cookie) {
    //Configurando tempo de vida da cookie ao executar funçao:
    var tempoCookie = new Date();
    tempoCookie.setTime(tempoCookie.getTime() + 1 * 3600 * 1000);

    //Definindo valores da funçao:
    var select = document.getElementById(idSelect);
    var option = select.options[select.selectedIndex];
    var recoje = select.options[select.selectedIndex].id;
    console.log(recoje);
    document.cookie = cookie + " = " + recoje + "; expires=" + tempoCookie.toUTCString();
}

function mudarCalendario(idCalendario, cookie) {
    var tempoCookie = new Date();
    tempoCookie.setTime(tempoCookie.getTime() + 1 * 3600 * 1000);

    //Definindo valores da funçao:
    var select = document.getElementById(idCalendario);
    var recoje = select.value;
    console.log(recoje);
    document.cookie = cookie + " = " + recoje + "; expires=" + tempoCookie.toUTCString();
}

function configCalendar(id, formatacao, view, data) {
    $('#' + id).datepicker({
        format: formatacao,
        viewMod: view,
        minViewMode: view,
        language: "pt-BR",
        zIndexOffset: 10000,
        //defaultDate: new Date()
    }).datepicker('update', data);
}

var dynamicColors = function() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
};