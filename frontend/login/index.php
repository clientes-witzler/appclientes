<?php
header('Content-type: text/html; charset=utf-8');

//$error = "";
//verificador da região do usuário:
function getUserIP()
{
  $client  = @$_SERVER['HTTP_CLIENT_IP'];
  $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
  $remote  = $_SERVER['REMOTE_ADDR'];
  if (filter_var($client, FILTER_VALIDATE_IP)) {
    $ip = $client;
  } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
    $ip = $forward;
  } else {
    $ip = $remote;
  }
  return $ip;
}
$user_ip = getUserIP();

/*
$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
$ipgeo = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
$pais = $ipgeo->geoplugin_countryName;
echo '<script>console.log("' . $pais . '");</script>';
if ($pais != "Brazil") {
  $exterior = 1;
}
*/
session_start();
if(!isset($_SESSION['conexao'])){
  $conexaoProd = "https://api.develop.clientes.witzler.com.br";
  $_SESSION['urlProd'] = $conexaoProd;
  $conexaoTestes = "http://localhost:8080";
  $_SESSION['conexao'] = "";
  if(isset($_SERVER['HTTP_HOST'])){
    if(strpos($_SERVER['HTTP_HOST'],"localhost") !== false){ // TESTES
      $_SESSION['conexao'] = $conexaoTestes;
    }else{ // PROD$_SESSION['conexao'].
      $_SESSION['conexao'] = $conexaoProd;
    }
  }else{ // FALLBACK DE CONEXÃO
    $_SESSION['conexao'] = $conexaoProd;
  }
}

if (isset($_SESSION['token'])) {
  header("Location: ../dashboard/index.php");
}

header("X-Frame-Options: SAMEORIGIN");
?>
<!DOCTYPE html>
<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<html lang="pt-BR">
<head>
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="Portal dos clientes do Grupo Witzler">
  <meta name="author" content="Grupo Witzler">
  <meta name="keyword" content="Mercado livre de Energia, Witzler Energia, Grupo Witzler">
  <title>Witzler Energia | Mercado Livre de Energia</title>
  <link rel="apple-touch-icon" sizes="57x57" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="60x60" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="72x72" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="76x76" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="114x114" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="120x120" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="144x144" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="152x152" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="180x180" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="icon" type="image/png" sizes="192x192" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="icon" href="../bibliotecas/icones/FAVICON/FAVICON_WITZLER_ENERGIA.ico">
  <!--<link rel="manifest" href="../bibliotecas/assets/favicon/manifest.json">-->

  <meta name="msapplication-TileColor" content="#0b1f50">
  <meta name="msapplication-TileImage" content="../bibliotecas/icones/FAVICON/FAVICON_WITZLER_ENERGIA.ico">
  <meta name="theme-color" content="#0b1f50">
  <!-- Main styles for this application-->
  <link href="../bibliotecas/css/style.css" rel="stylesheet">
  <!-- CSS PARA TEMA GERAL -->
  <style>
    .btn.btn-link,a,a:hover{color:#000FFF}.btn.btn-link,a:focus{box-shadow:none}.btn.btn-primary{background-color:#0b1f50;color:#fff;border-color:rgb(4.8351648352, 13.6263736264, 35.1648351648)}.btn-primary.focus,.btn-primary:focus,.btn-primary:not(:disabled):not(.disabled):active,.btn-primary:not(:disabled):not(.disabled):active:focus{background-color:rgb(4.8351648352, 13.6263736264, 35.1648351648);border-color:rgb(4.8351648352, 13.6263736264, 35.1648351648);color:#fff;box-shadow:0 0 0 .2rem rgba(4.8351648352, 13.6263736264, 35.1648351648,0.5)}.btn.btn-secondary{background-color:#000FFF;color:#fff;border-color:#000CCC}.btn-secondary.focus,.btn-secondary:focus,.btn-secondary:not(:disabled):not(.disabled):active,.btn-secondary:not(:disabled):not(.disabled):active:focus{background-color:#000CCC;border-color:#000CCC;color:#fff;box-shadow:0 0 0 .2rem rgba(0,12,204,.5)}form input.form-control:focus{box-shadow:0 0 0 .2rem rgb(4.8351648352, 13.6263736264, 35.1648351648,.5);border-color:#0b1f50}
    .alert--custom{
      position:fixed;
      bottom:12px;
      max-width:900px;
      margin:0;
      left:calc(50% - 450px);
      box-shadow:0 0 8px rgba(0,0,0,0.5);
      z-index:2;
      display:flex;
      align-items:center;
      background-color:rgba(11, 31, 80, 0.8);
      color:#FFF;
      border-color:#0b1f50;
    }

    .alert--custom a{
      color:#ffc741;
      font-weight:bold;
    }
    .alert--custom .box-icone{
      font-size:36px;
      width:1em;
      height:1em;
      line-height:1em;
      text-align:center;
      margin-right:18px;
      min-width:1em;
    }
  </style>

  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css'>
  <meta name="robots" content="noindex">
  <script>
    /*window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');*/
  </script>
</head>

<body class="c-app flex-row align-items-center" style="background-image: url(../bibliotecas/assets/img/background-3.9902c0af.jpg); background-repeat: no-repeat; background-attachment: fixed; background-position: center; ">
  <div class="alert alert--custom animated fadeIn">
    <span class="box-icone">
      <svg id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"></path>
      </svg>
    </span>
    <span>Utilizamos cookies para melhorar a sua experiência no site. Ao continuar navegando, você concorda com a nossa <a href="../../politica_privacidade_grupo_witzler.pdf">Política de Privacidade</a> e com a utilização de cookies no navegador.</span>
  </div>
  <div class="wrapper">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-5">
          <div class="login animated slideInDown card-group">
            <div class="card p-4" style="background-color: rgba(255, 255, 255, 0.8);">
              <div class="card-body">
                <!--<h1>Login</h1>
                <p class="text-muted">Entre na sua conta</p>-->
                <div style="margin-top: -10px; padding-bottom: inherit; width:80%;max-width:300px;">
                  <img src="../bibliotecas/assets/img/logo_grupo_witzler.svg" style="padding-left: 65px;height:auto;max-width:100%;">
                </div>
                <form method="post" id="formulario-login" id="formulario-login" action="login.php">
                  <div class="input-group mb-3" style="padding-left: 25px; padding-right: 40px;">
                    <div class="input-group-prepend"><span class="input-group-text">
                        <img src="../bibliotecas/icones/ICONES_USUARIO.svg" height=16 width=16></span></div>
                    <input class="form-control" type="text" placeholder="Usuario" name="usuario" id="usuario" autocomplete="off">
                  </div>


                  <div class="input-group mb-4" style="padding-left: 25px; padding-right: 40px;">
                    <div class="input-group-prepend"><span class="input-group-text">
                        <img src="../bibliotecas/icones/ICONES_SENHA.svg" height=16 width=16></span></div>
                    <input class="form-control" type="password" placeholder="Senha" name="senha" id="senha" required>
                  </div>
                  <input type="hidden" name="ip" value="" id="ipusuario"/>
                  <div class="row">
                    <div class="col-6 " style="padding-left: 40px;">
                      <button class="btn btn-link px-0" type="button" onclick="window.location.href='../esqueci-senha/index.php'">Esqueceu sua senha?</button>
                    </div>
                    <div class="col-6 text-right" style="padding-right: 55px;">
                      <button class="login-btn btn btn-primary px-4" type="submit" name="login" id="login" value="login" <?php /*if (($exterior)) {
                                                                                                                  echo "disabled";
                                                                                                                }*/ ?>>Login</button>
                    </div>
                  </div>
                </form>

                <?php
                if (isset($_GET['erro']) && $_GET['erro'] == "login") {
                  echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Essa conta não existe<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                } else if (isset($_GET['erro']) && $_GET['erro'] == "nologin") {
                  echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Voce nao esta logado<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                } else if (isset($_GET['erro']) && $_GET['erro'] == "sessaoexpirou") {
                  echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">A sessão expirou<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                } else if (isset($_GET['erro']) && $_GET['erro'] == "containvalida") {
                  echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">A conta logada não é válida.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                } else if (isset($_GET['erro']) && $_GET['erro'] == "senhaAlterada" && isset($_GET['emailEnviado'])) {
                  echo '<br/><div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">A nova senha foi enviada com sucesso ao e-mail: '.$_GET['emailEnviado'].'<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                } else if (isset($_GET['erro']) && $_GET['erro'] == "recuperarSenha"){
                  echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Não encontramos um email para recuperação, por favor, entre em contato com o suporte.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                } else if (isset($exterior)) {
                  echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Somente são permitidos usuarios do Brasil.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                }

                ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- CoreUI and necessary plugins-->
  <script src="../bibliotecas/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
  <!--[if IE]><!-->
  <script src="../bibliotecas/vendors/@coreui/icons/js/svgxuse.min.js"></script>
  <!--<![endif]-->

  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
  <script src="./script.js"></script>
  <script>
  $.getJSON('https://api.ipify.org?format=jsonp&callback=?', function(data) {
    //console.log(JSON.stringify(data, null, 2));
    //console.log(data.ip);
    document.getElementById('ipusuario').value = data.ip;
  });
  </script>
</body>

</html>
