<?php
    //verificador da região do usuário:
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://ipinfo.io/json');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-type: application/json',
        'Accept: */*',
        'Connection: keep-alive'
    ));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate,sdch');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $resposta = curl_exec($ch);
    $ipgeo = "";
    if(str_contains($resposta,'{') && str_contains($resposta,'}')){ // CASO JSON VÁLIDO
        $ipgeo = @json_decode($resposta);
        $pais = $ipgeo->country;
        if($pais != "BR"){
            $exterior = 1;
        }
    }else{
        echo("JSON inválido.");
    }
    if(curl_errno($ch)){ // verificação se existe algum erro na conexão do Curl
        $mensagem_erro = curl_error($ch);
    }
    curl_close($ch);
    if(isset($mensagem_erro)){
        echo('Erro ao verificar o IP de origem desta máquina.');
    }
?>