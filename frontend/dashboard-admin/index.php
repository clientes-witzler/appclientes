<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


  <!-- HEAD DA PAGINA -->
  <?php include "../recursos/head.php"; ?>
  <!-- BODY DA PAGINA -->
  <body class="c-app">
        <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
        <?php include "../recursos/menu-admin.php"; ?>
  
        <div class="c-subheader px-3">
          <!-- Breadcrumb-->
          <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Atualizações</li>
          </ol>
        </div>
      <!-- Fim do cabeçalho -->
      <div class="c-body">
        <main class="c-main">
          <div class="container-fluid">
            <div class="fade-in">

              <!-- CARD DE MEDICAO MENSAL DE CONSUMO -->
              <?php include "div/divAdmin.php"; ?>

            </div>
          </div>
        </main>
      </div>
      <!-- INCLUDE DE CONFIG DO AJAX -->
      <?php include "div/configAjax.php"; ?>
    <!-- FOOTER -->
    <?php include "../recursos/footer.php"; ?>