<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2024 Equipe de Desenvolvimento.
-->


<!-- HEAD DA PAGINA -->
<?php include "../recursos/head.php"; ?>
<!-- BODY DA PAGINA -->

<body class="c-app">
    <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
    <?php
    include "../recursos/menu.php";
    //Variavel que mudara o config do select
    //$meus_arquivos = 1;
    ?>

    <div class="c-subheader px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">FAQ</li>
        </ol>
    </div>
    <!-- Fim do cabeçalho -->
    <!--Començo do conteudo do site-->
    <br />
    <div class="container-fluid pt-1">
        <?php
            // $requisicaoIframeMovidesk = curl_init();
            // curl_setopt($requisicaoIframeMovidesk, CURLOPT_URL, 'https://wtz.movidesk.com/kb/article/436482/conheca-a-sua-base-de-conhecimento');
            // curl_setopt($requisicaoIframeMovidesk, CURLOPT_HTTPHEADER, [
            //     'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'
            // ]);
            // curl_setopt($requisicaoIframeMovidesk, CURLOPT_CONNECTTIMEOUT, 2);
            // curl_setopt($requisicaoIframeMovidesk, CURLOPT_RETURNTRANSFER, 1);
            // curl_setopt($requisicaoIframeMovidesk, CURLOPT_USERAGENT, 'Witzler Energia');
            // $iframeMovidesk = curl_exec($requisicaoIframeMovidesk);
            // curl_close($requisicaoIframeMovidesk);
            // echo($iframeMovidesk);
        ?>
        <iframe id="iframe-movidesk" class="embed-responsive" width="1920" height="800" src="https://wtz.movidesk.com/kb/article/436482/conheca-a-sua-base-de-conhecimento?menuId=43045-117441-436482&ticketId=&q=" frameborder="0"></iframe>
    </div>

    
    <!-- FIM DO CONTEUDO DO SITE -->
    <!-- FOOTER -->
    <?php include "../recursos/footer.php"; ?>