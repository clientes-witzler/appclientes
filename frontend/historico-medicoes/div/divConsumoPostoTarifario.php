<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php include "../../backend/graficos/consumoPostoTarifario/pegarUltimoRegistro/recolhe.php"; ?>

<div class="text-value card-header">
   <div class="row">
      <div class="card-header-row col-12 col-sm-4 col-lg-2 text-center d-flex justify-content-center">Consumo por Posto Tarifário</div>
      <div class="card-header-row col-lg-6 col-sm-4"></div>
      <div class="card-header-row arquivoInput col-12 col-sm-4 col-lg-4" style="text-align: right;">
         <div class="card-header-month">
            <!-- CALENDARIO -->
            <?php include "../../backend/datas/datasConsumoPostoTarifario/calendarioConsumoPostoTarifario.php"; ?>
         </div>
      </div>
   </div>
</div>
<div class="card-body">
   <div class="chartWrapper">
      <div class="chartAreaWrapper" id="updateConsumoPostoTarifario" style="height: 422px;">
         <script>
            $("#updateConsumoPostoTarifario").html("<div id='loadConsumoPostoTarifario' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
            setTimeout(function() {
               document.getElementById('loadConsumoPostoTarifario').remove();
               document.getElementById('chartDoughnutPostoTarifario').style.display = 'block';
            }, 2500);
         </script>

         <canvas class="mx-auto chartjs-render-monitor" id="chartDoughnutPostoTarifario" width="1107" height="269" style="display: none; height: 300px; width: 1231px;"></canvas>
         <?php
         include "../../backend/graficos/consumoPostoTarifario/graficoPostoTarifario.php";
         include "../../backend/graficos/consumoPostoTarifario/chartConsumoPostoTarifario.php";
         //Formatacao das variaveis em url:
         $data = isset($ultimaData) ? $ultimaData : date("Y-m-d");
         $idUnidadde = isset($_COOKIE['id_unidades']) ? $_COOKIE['id_unidades'] : $arrayIdOption[0];
         $postoTarifario = new PostoTarifario("chartDoughnutPostoTarifario", $data, $idUnidadde);
         ?>
         <script>
            <?php
            // $postoTarifario->varChartData("chartPostoTarifario", array("Ponta (KWh)", "Fora Ponta (KWh)"), array("ponta", "foraPonta"), array("rgba(76, 142, 173, 0.7)", "rgba(29, 107, 170, 0.7)"));
               $postoTarifario->varChartData("chartPostoTarifario", array("Ponta (KWh)", "Fora Ponta (KWh)"), array("ponta", "foraPonta"), array($_SESSION['cor-custom-2'], $_SESSION['cor-custom-1']));
               $postoTarifario->configChartData("chartPostoTarifario", "doughnut", "Consumo por Posto Tarifário");
            ?>
         </script>
      </div>
   </div>
</div>