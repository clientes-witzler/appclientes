<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php include "../../backend/graficos/fatorPotencia/pegarUltimoRegistro/recolhe.php"; ?>

<div class="text-value card-header">
   <div class="row">
      <div class="card-header-row col-12 col-sm-3 col-lg-2 text-center d-flex justify-content-center align-items-center">Fator de Potência</div>
      <div class="col-md-12 col-sm-12 col-lg-6 col-xs-12 input-group arquivoInput">

         <div class="input-group-prepend col-lg-5 col-md-12 col-sm-12 col-xs-12" style="padding: 0;">
            <?php
            include "../../backend/cardsDashboard/capacitivoIndutivoFatorPotencia.php";
            ?>
            <span class="input-group-text" id="basic-addon3">CAPACITIVO</span>
            <input style="background: white;" type="text" readonly class="form-control" id="input_capacitivo" aria-describedby="basic-addon3" value=<?php if (isset($mediaFormatadaAtual)) {
                                                                                                                                                         if (is_nan($mediaAtual)) {
                                                                                                                                                            echo "0";
                                                                                                                                                         } else {
                                                                                                                                                            echo $mediaFormatadaAtual;
                                                                                                                                                         }
                                                                                                                                                      } else {
                                                                                                                                                         echo "0";
                                                                                                                                                      }
                                                                                                                                                      ?>>
         </div><br />
         <div class="input-group-prepend col-lg-5 col-md-12 col-sm-12 col-xs-12" style="padding: 0;">
            <span class="input-group-text" id="basic-addon3">INDUTIVO</span>
            <input style="background: white;" type="text" readonly class="form-control" id="input_indutivo" aria-describedby="basic-addon3" value=<?php

                                                                                                                                                   if (isset($mediaFormatadaIndutivaAtual)) {
                                                                                                                                                      if (is_nan($mediaIndutivaAtual)) {
                                                                                                                                                         echo "0";
                                                                                                                                                      } else {
                                                                                                                                                         echo $mediaFormatadaIndutivaAtual;
                                                                                                                                                      }
                                                                                                                                                   } else {
                                                                                                                                                      echo "0";
                                                                                                                                                   }
                                                                                                                                                   ?>>
         </div>

      </div>
      <!--<div class="card-header-row col-lg-6 col-sm-5"></div>-->

      <div class="card-header-row arquivoInput col-12 col-sm-4 col-lg-4" style="text-align: right;">

         <div class="card-header-date">
            <!-- CALENDARIO -->
            <?php include "../../backend/datas/datasFatorPotencia/calendarioFatorPotencia.php"; ?>
         </div>
      </div>
   </div>
</div>
<div class="card-body">
   <div class="chartWrapper" style="position: relative;">
      <div class="chartAreaWrapper" id="updateFatorPotencia" style="overflow-x: auto; height: 400px;">
         <script>
            $("#updateFatorPotencia").html("<div id='loadFatorPotencia' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
            setTimeout(function() {
               document.getElementById('loadFatorPotencia').remove();
               document.getElementById('chartBarFatorPotencia').style.display = 'block';
            }, 2500);
         </script>

         <canvas id="chartBarFatorPotencia" class="fade-in" width="1540" height="380" style="display: none; position: relative;"></canvas>
         <?php
         //include "../../backend/graficos/fatorPotencia/graficoFatorPotencia.php";
         //include "../../backend/graficos/fatorPotencia/chartFatorPotencia.php";
         $dataFatorPotencia = isset($_COOKIE['data_fator_potencia']) ? $_COOKIE['data_fator_potencia'] : $ultimaData;
         $idUnidadesFatorPotencia = isset($_COOKIE['id_unidades']) ? $_COOKIE['id_unidades'] : $arrayIdOption[0];
         $fatorPotencia = new FatorPotencia("chartBarFatorPotencia", $dataFatorPotencia, $idUnidadesFatorPotencia);
         ?>
         <script>
            <?php
            $fatorPotencia->varChartData("chartFatorPotencia", array("Fator de potencia", "Limite indutivo", "Limite capacitivo"), array("capacitivo"), array($_SESSION['cor-custom-9']), array($_SESSION['cor-custom-10'], $_SESSION['cor-custom-10']));
            $fatorPotencia->configChartData("chartFatorPotencia", "bar", "Consumo por Posto Tarifário");
            ?>
         </script>
      </div>
   </div>
   </pre>
</div>