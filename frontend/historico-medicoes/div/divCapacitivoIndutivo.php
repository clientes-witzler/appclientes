<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->



<div class="card-header">
        <div class="row d-flex">
        
            <div class="card-header-row col-12 col-sm-6 col-lg-4 text-center d-flex justify-content-center align-items-center">
                <div class="mb-0 header-chart Col card-title col-12 text-center text-lg-right text-xl-right">
                    <label class="text-value" style="margin-top: 10px;">
                        Fator de Potência Mensal: <?php 
                        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                        date_default_timezone_set('America/Sao_Paulo');
                        echo utf8_encode(ucfirst(strftime('%B/%Y', strtotime('today')))); 
                    ?>
                    </label>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-8 col-xs-12 input-group arquivoInput"> 
            
                <div class="input-group-prepend col-lg-5 col-md-12 col-sm-12 col-xs-12" style="padding: 0;">
                <?php
                include "../../backend/cardsDashboard/acumuladoMensal.php";
            ?>
                    <span class="input-group-text" id="basic-addon3">CAPACITIVO</span>
                    <input style="background: white;" type="text" readonly class="form-control" id="basic-url" aria-describedby="basic-addon3" value=
                                <?php if (isset($mediaFormatada)) {
                                          if (is_nan($media)) {
                                             echo "--";
                                          } else {
                                             echo $mediaFormatada;
                                          }
                                       } else {
                                          echo "--";
                                       }
                                ?>
                    >
                </div><br/>
                <div class="input-group-prepend col-lg-5 col-md-12 col-sm-12 col-xs-12" style="padding: 0;">
                    <span class="input-group-text" id="basic-addon3">INDUTIVO</span>
                    <input style="background: white;" type="text" readonly class="form-control" id="basic-url" aria-describedby="basic-addon3" value=
                    
                        <?php 
                            if (isset($mediaFormatadaIndutiva)) {
                                if (is_nan($mediaIndutiva)) {
                                    echo "--";
                                } else {
                                    echo $mediaFormatadaIndutiva;
                                }
                            } else {
                               echo "--";
                                       } 
                        ?>
                        >
                </div>
                
            </div>

            <!--<div class="card-header-row col-12 col-sm-3 col-lg-2 text-center d-flex justify-content-center align-items-center">
                <h5>
                    <?php 
                        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                        date_default_timezone_set('America/Sao_Paulo');
                        echo utf8_encode(ucfirst(strftime('%B', strtotime('today')))); 
                    ?>
                </h5>
            </div>-->
            

        </div>
        
</div>
    