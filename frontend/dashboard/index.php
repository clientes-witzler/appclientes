<!-- 
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<!-- HEAD DA PAGINA -->
<?php include "../recursos/head.php"; ?>
<!-- BODY DA PAGINA -->

<style>
  @media(min-width:992px) and (max-width:1700px){

    .col-lg-3{
      max-width:50% !important;
      flex: inherit;
    }

    .col-lg-3 .card-body{
      overflow:hidden;
    }

    .c-main > .container-fluid > .fade-in > .row .bodyUsuarioCo2{
      width:100%;
      max-width:100%;
      display:block;
    }

    .c-main > .container-fluid > .fade-in > .row,
    .c-main > .container-fluid > .fade-in > .row > * .row{
      justify-content:center;
      text-align:center;
    }

    .c-main > .container-fluid > .fade-in > .row > *{
      flex:inherit;
      max-width:100%;
      width:100%;

    }

  }
</style>

<body class="c-app">
  <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
  <?php include "../recursos/menu.php"; ?>

  <div class="c-subheader px-3">
    <!-- Breadcrumb-->
    <ol class="breadcrumb border-0 m-0">
      <li class="breadcrumb-item">Atualizações</li>
    </ol>
  </div>
  <!-- Fim do cabeçalho -->
  <div class="c-body">
    <main class="c-main">
      <div class="container-fluid">
        <div class="fade-in">
          <!-- SE NAO QUEREMOS QUE ARMAZENE OS VALORES AO RECARREGAR, SO TIRAR ESTE COMENTARIO COM O INCLUDE -->
          <?php 
            if(isset($_SESSION['sem-medicao'])){
            }else{
              include "div/pegarUltimoRegistro.php";
            }
          ?>
          <!-- ============================================================================================= -->
          <!-- NOVO -->
          <?php
            if(!isset($_SESSION['sem-medicao'])){
              include "../../backend/economiaUnidade/listarEconomiaunidade.php";
              include "div/divCardEconomiaMercadoLivre.php";
              // <!---------->
              // <!-- CARD DE MEDICAO MENSAL DE CONSUMO -->
              include "div/divPonta.php";
              // <!-- GRUPO DE CARD: acumulado mensal, fatores potencia -->
              include "div/divCardsUpdate.php";
              // <!-- Segundo quadro -->
              //include "div/divEconomiaConsolidado.php"; 
              //include "div/divHistoricoEconomiaUnidade.php"; 
               include "div/divHistoricoEconomiaUnidade.php";
            }else{
              include "div/divCardEconomiaMercadoLivre.php";
              echo("
                  <div class='box-sem-medicao animation fade-in' style='background-color:rgba(17,55,89,0.9);text-align:center;color:#FFF;box-shadow: 0 0 8px rgba(0,0,0,0.5)'>
                      <div style='height:70px'></div>
                      <span style='font-size:30px;'>Infelizmente, sua unidade ainda não registrou nenhuma medição,<br> aguarde até o começo do próximo mês.</span>
                      <div style='height:70px'></div>
                  </div>
              ");
            }
          ?>
        </div>
      </div>
    </main>
  </div>
  <!-- INCLUDE DE CONFIG DO AJAX -->
  <?php include "div/configAjax.php"; ?>
  <script>
    var last_id_unidades = $("#idUnidades").find(':selected').attr('id');
    var last_calendario_unidade = "01-" + $("#datasCalendario").val();
    var current_calendario_unidade = "01-" + $("#datasCalendario").val();
    var anoNovo = last_calendario_unidade.substr(6, 10);
    var mesNovo = last_calendario_unidade.substr(3, 2);
    var finalNovo = new Date(anoNovo, mesNovo, 0);
    var ultimoDia = finalNovo.getDate();
    var dataFinalFormatada = anoNovo + '-' + mesNovo + '-' + ultimoDia;

    $.ajax({
        type: 'GET',
        url: '../../backend/cardsDashboard/updates/updateDemandasCard.php',
        data: {
            'id_unidades': last_id_unidades,
            'mes_ref': dataFinalFormatada
        },
        success: function(s) {
            $("#updateDemandasCard").html(s);
            console.log(s);
        }
    });
  </script>
  <!-- FOOTER -->
  <?php include "../recursos/footer.php"; ?>
