<?php
class Graficos
{
    // Atributos:
    private $id;
    private $id_element;
    private $config;
    private $labels;
    private $data;

    // Metodos especiais:
    public function __construct($id, $id_element, $labels, $data, $config)
    {
        $this->setId($id);
        $this->setIdElement($id_element);
        $this->setLabels($labels);
        $this->setData($data);
        $this->setConfig($config);
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */
    public function setId($id): self
    {
        $this->id = validateValue($id);

        return $this;
    }

    /**
     * Get the value of id_element
     */
    public function getIdElement()
    {
        return $this->id_element;
    }

    /**
     * Set the value of id_element
     */
    public function setIdElement($id_element): self
    {
        $this->id_element = validateValue($id_element);

        return $this;
    }

    /**
     * Get the value of config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set the value of config
     */
    public function setConfig($config): self
    {
        $this->config = validateValue($config);

        return $this;
    }

    /**
     * Get the value of labels
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * Set the value of labels
     */
    public function setLabels($labels): self
    {
        $this->labels = validateValue($labels);

        return $this;
    }

    /**
     * Get the value of data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     */
    public function setData($data): self
    {
        $this->data = validateValue($data);

        return $this;
    }

    // Metodos publicos:
    public function validateCriarGrafico()
    {
        $id = $this->getId();
        $id_element = $this->getIdElement();
        $labels = $this->getLabels();
        $data = $this->getData();
        $config = $this->getConfig();

        if ($id && is_null($id)) {
            // Validate id
            return 'Invalid id';
        } else if ($id_element && is_null($id_element)) {
            // Validate id_element
            return 'Invalid id_element';
        } else if ($labels && is_null($labels)) {
            // Validate labels
            return 'Invalid labels';
        } else if ($data && is_null($data)) {
            // Validate data
            return 'Invalid data';
        } else if ($config && is_null($config)) {
            // Validate config
            return 'Invalid config';
        } else {
            return 200;
        }
    }
}
