<?php
class MessagesController
{
    public function message($status, $msg = "")
    {
        // Envia valor pro model, sanitiza e verifica se ainda tem valor:
        $model = new Messages($status, $msg);
        $validade = $model->validateValues();
        if ($validade == 200) {
            // Se chegou até aqui, significa que o valor existe mesmo depois da validação, recolhe valor do msg e retorna
            $status = $model->getStatus();
            $msg = $model->getMsg();
            $view = new MessagesView();
            $response = $view->showMsg($status, $msg);
            return $response;
        } else {
            // A sanitização deu null, então não retorna msg:
            $view = new MessagesView();
            $response = $view->showMsg("", "");
            return $response;
        }
    }
}
