<?php
class PgConnectController
{
    public function selectQuery($newQuery, $params = array())
    {
        $model = new PgConnect();
        $view = new PgConnectView();

        $stmt = $model->newQuery($newQuery, $params);
        $validate = $view->validateQuery($stmt);

        if (isset($validate["idstatus"]) && $validate["idstatus"] == 200) {
            return $view->validateFetchAll($stmt->fetchAll(PDO::FETCH_ASSOC));
        } else {
            // Aconteceu um erro ao executar a query, entao retorna como msg de erro e seu status:
            var_dump($validate);
        }
    }

    public function insertQuery($newQuery, $params = array())
    {
        $model = new PgConnect();
        $view = new PgConnectView();

        $stmt = $model->newQuery($newQuery, $params);

        $validate = $view->validateQuery($stmt);

        return $validate;
    }

    public function updateQuery($newQuery, $params = array())
    {
        $model = new PgConnect();
        $view = new PgConnectView();

        $stmt = $model->newQuery($newQuery, $params);
        $validate = $view->validateRowsAffected($stmt);

        return $validate;
    }
}
