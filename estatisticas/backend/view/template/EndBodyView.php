<?php
class EndBodyView
{
    // SCRIPT E CONFIGS PARA ADM
    public function generateAdmin($ruta, $addConfig)
    {
        echo "
            <!-- Optional JavaScript -->
            <!-- bootstap bundle js -->
            <script src=\"$ruta/assets/vendor/bootstrap/js/bootstrap.bundle.js\"></script>
            <!-- slimscroll js -->
            <script src=\"$ruta/assets/vendor/slimscroll/jquery.slimscroll.js\"></script>
            <!-- main js -->
            <script src=\"$ruta/assets/libs/js/main-js.js\"></script>
            <!-- chart js (graficos) -->
            <!--<script src=\"$ruta/assets/libs/js/chartjs.js\"></script>-->
            <script src=\"https://cdn.jsdelivr.net/npm/chart.js\"></script>
            
            <script src=\"$ruta/assets/libs/js/ajax-functions.js\"></script>
            
            $addConfig
        ";
    }

    // SCRIPT E CONFIGS PARA USER
    public function generateUser($ruta, $addConfig)
    {
        echo "
            <!-- Optional JavaScript -->
            <!-- bootstap bundle js -->
            <script src=\"$ruta/assets/vendor/bootstrap/js/bootstrap.bundle.js\"></script>
            <!-- slimscroll js -->
            <script src=\"$ruta/assets/vendor/slimscroll/jquery.slimscroll.js\"></script>
            <!-- main js -->
            <script src=\"$ruta/assets/libs/js/main-js.js\"></script>
            <!-- chart chartist js -->
            <script src=\"$ruta/assets/vendor/charts/chartist-bundle/chartist.min.js\"></script>
            <!-- sparkline js -->
            <script src=\"$ruta/assets/vendor/charts/sparkline/jquery.sparkline.js\"></script>
            <!-- morris js -->
            <script src=\"$ruta/assets/vendor/charts/morris-bundle/raphael.min.js\"></script>
            <script src=\"$ruta/assets/vendor/charts/morris-bundle/morris.js\"></script>
            <!-- chart c3 js -->
            <script src=\"$ruta/assets/vendor/charts/c3charts/c3.min.js\"></script>
            <script src=\"$ruta/assets/vendor/charts/c3charts/d3-5.4.0.min.js\"></script>
            <script src=\"$ruta/assets/vendor/charts/c3charts/C3chartjs.js\"></script>
            <script src=\"$ruta/assets/libs/js/dashboard-ecommerce.js\"></script>
            
            <script src=\"$ruta/assets/libs/js/ajax-functions.js\"></script>
        ";
    }
}
