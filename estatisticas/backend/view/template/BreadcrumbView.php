<?php
class BreadcrumbView
{
    // SCRIPT E CONFIGS PARA ADM
    public function generateAdmin($title, $text, $listItem, $ruta, $addConfig)
    {
        echo "
            <div class=\"row\">
                <div class=\"col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12\">
                    <div class=\"page-header\">
                        <h2 class=\"pageheader-title\">$title </h2>
                        <p class=\"pageheader-text\">$text</p>
                        <div class=\"page-breadcrumb\">
                            <nav aria-label=\"breadcrumb\">
                                <ol class=\"breadcrumb\">
        ";
        $this->listaLinks($listItem, $ruta);
        echo "
                                </ol>
                            </nav>
                        </div>
                        $addConfig
                    </div>
                </div>
            </div>
        ";
    }

    // SCRIPT E CONFIGS PARA USER
    public function generateUser($title, $text, $listItem, $ruta, $addConfig)
    {
        echo "
            <div class=\"row\">
                <div class=\"col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12\">
                    <div class=\"page-header\">
                        <h2 class=\"pageheader-title\">$title </h2>
                        <p class=\"pageheader-text\">$text</p>
                        <div class=\"page-breadcrumb\">
                            <nav aria-label=\"breadcrumb\">
                                <ol class=\"breadcrumb\">
        ";
        $this->listaLinks($listItem, $ruta);
        echo "
                                </ol>
                            </nav>
                        </div>
                        $addConfig
                    </div>
                </div>
            </div>
        ";
    }

    // Function que gera lista de breadcrumb-item:
    public function listaLinks($listItem, $ruta)
    {
        $totItem = count($listItem) ? count($listItem) : 0;

        if ($totItem > 0) {
            // Sanitiza cada item passado
            for ($i = 0; $i < $totItem; $i++) {
                $value = isset($listItem[$i]) && $listItem[$i] != "" ? validateValue($listItem[$i]) : "N/D";
                $ativo = $i + 1 == $totItem ? "active" : "";

                echo "<li class=\"breadcrumb-item $ativo\"><a href=\"#\" class=\"breadcrumb-link\">$value</a></li>";
            }
        }
    }
}
