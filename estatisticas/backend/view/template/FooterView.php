<?php
class FooterView
{
    // SCRIPT E CONFIGS PARA ADM
    public function generateAdmin($ruta, $addConfig)
    {
        $ano = date("Y");
        echo "
            <div class=\"footer\">
                <div class=\"container-fluid\">
                    <div class=\"row\">
                        <div class=\"col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12\">
                            Copyright © $ano Concept. All rights reserved. Dashboard by <a href=\"https://colorlib.com/wp/\">Colorlib</a>.
                        </div>
                        <div class=\"col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12\">
                            <div class=\"text-md-right footer-links d-none d-sm-block\">
                                <a href=\"javascript: void(0);\">About</a>
                                <a href=\"javascript: void(0);\">Support</a>
                                <a href=\"javascript: void(0);\">Contact Us</a>
                                $addConfig
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ";
    }

    // SCRIPT E CONFIGS PARA USER
    public function generateUser($ruta, $addConfig)
    {
        $ano = date("Y");
        echo "
            <div class=\"footer\">
                <div class=\"container-fluid\">
                    <div class=\"row\">
                        <div class=\"col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12\">
                            Copyright © $ano Concept. All rights reserved. Dashboard by <a href=\"https://colorlib.com/wp/\">Colorlib</a>.
                        </div>
                        <div class=\"col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12\">
                            <div class=\"text-md-right footer-links d-none d-sm-block\">
                                <a href=\"javascript: void(0);\">About</a>
                                <a href=\"javascript: void(0);\">Support</a>
                                <a href=\"javascript: void(0);\">Contact Us</a>
                                $addConfig
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ";
    }
}
