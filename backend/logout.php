<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//Apagando cookies
if(isset($_COOKIE['id_unidades'])){
    unset($_COOKIE['id_unidades']);
}
if(isset($_COOKIE['calendario_unidade'])){
    unset($_COOKIE['calendario_unidade']);
}
if(isset($_COOKIE['final_mes'])){
    unset($_COOKIE['final_mes']);
}
if(isset($_COOKIE['data_calendario'])){
    unset($_COOKIE['data_calendario']);
}
if(isset($_COOKIE['ano_novo_calendario'])){
    unset($_COOKIE['ano_novo_calendario']);
}
if(isset($_COOKIE['final_mes_calendario'])){
    unset($_COOKIE['final_mes_calendario']);
}
if(isset($_COOKIE['ultimo_dia_calendario'])){
    unset($_COOKIE['ultimo_dia_calendario']);
}
if(isset($_COOKIE['mes_novo_calendario'])){
    unset($_COOKIE['mes_novo_calendario']);
}
if(isset($_COOKIE['data_consumo_dia'])){
    unset($_COOKIE['data_consumo_dia']);
}
if(isset($_COOKIE['data_consumo_posto_tarifario'])){
    unset($_COOKIE['data_consumo_posto_tarifario']);
}
if(isset($_COOKIE['final_mes_consumo_posto_tarifario'])){
    unset($_COOKIE['final_mes_consumo_posto_tarifario']);
}
if(isset($_COOKIE['data_fator_potencia'])){
    unset($_COOKIE['data_fator_potencia']);
}
if(isset($_COOKIE['tipo_arquivo'])){
    unset($_COOKIE['tipo_arquivo']);
}
if(isset($_COOKIE['data_unidade'])){
    unset($_COOKIE['data_unidade']);
}
if(isset($_COOKIE['data_tipo_arquivo_cliente'])){
    unset($_COOKIE['data_tipo_arquivo_cliente']);
}
if(isset($_COOKIE['id_colaborador'])){
    unset($_COOKIE['id_colaborador']);
}
if(isset($_COOKIE['admin_colaborador'])){
    unset($_COOKIE['admin_colaborador']);
}
setcookie('id_unidades', null, -1, '/','',true,true);
setcookie('calendario_unidade', null, -1, '/','',true,true);
setcookie('final_mes', null, -1, '/','',true,true);
setcookie('data_caledario', null, -1, '/','',true,true);
setcookie('ano_novo_calendario', null, -1, '/','',true,true);
setcookie('final_mes_calendario', null, -1, '/','',true,true);
setcookie('ultimo_dia_calendario', null, -1, '/','',true,true);
setcookie('mes_novo_calendario', null, -1, '/','',true,true);
setcookie('data_consumo_dia', null, -1, '/','',true,true);
setcookie('data_consumo_posto_tarifario', null, -1, '/','',true,true);
setcookie('final_mes_consumo_posto_tarifario', null, -1, '/','',true,true);
setcookie('data_fator_potencia', null, -1, '/','',true,true);
setcookie('tipo_arquivo', null, -1, '/','',true,true);
setcookie('data_unidade', null, -1, '/','',true,true);
setcookie('data_tipo_arquivo_cliente', null, -1, '/','',true,true);
setcookie('id_colaborador', null, -1, '/','',true,true);
setcookie('admin_colaborador', null, -1, '/','',true,true);

//Apagando sessao php
session_start();
session_destroy();
header("Location: ../frontend/login/index.php?q=logout");
exit();
?>