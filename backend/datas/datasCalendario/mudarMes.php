<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    //Retrocede 1 mes:
    $('#previous').click(function(){
        var data_atual = document.getElementById('datasCalendario');
        var recolhe_data = '01-'+data_atual.value;
        //console.log(recolhe_data);

        //Recorta as datas para pegar apenas os valores necessarios:
        /*var ano = recolhe_data.substring(4, 0);
        var mes = recolhe_data.substring(5, 7);*/
        var ano = recolhe_data.substring(6, 10);
        var mes = recolhe_data.substring(5, 3);

        //Reduz o valor da data em 1 e formata se for necessario:
        mes--;
        if(mes < 10){
            mes = "0"+mes;
        }
        
        //Verifica se reduziu o suficiente pra mudar de ano:
        if(mes == "00"){
            mes = 12;
            ano--;
        }
        //Monta a data com os novos valores:
        //var data_nova = ano + "-" + mes + "-01";
        var data_nova = mes+"-"+ano;
        //console.log(data_nova);

        //Insere no input e faz update dele:
        $("#datasCalendario").val(data_nova);
    })
</script>
<script>
    //Retrocede 1 mes:
    $('#next').click(function(){
        var data_atual = document.getElementById('datasCalendario');
        var recolhe_data = '01-'+data_atual.value;
        //console.log(recolhe_data);

        //Recorta as datas para pegar apenas os valores necessarios:
        /*var ano = recolhe_data.substring(4, 0);
        var mes = recolhe_data.substring(5, 7);*/
        var ano = recolhe_data.substring(6, 10);
        var mes = recolhe_data.substring(5, 3);

        //Reduz o valor da data em 1 e formata se for necessario:
        mes++;
        if(mes < 10){
            mes = "0"+mes;
        }
        
        //Verifica se reduziu o suficiente pra mudar de ano:
        if(mes == "13"){
            mes = "01";
            ano++;
        }
        //Monta a data com os novos valores:
        //var data_nova = ano + "-" + mes + "-01";
        var data_nova = mes+"-"+ano;
        //console.log(data_nova);

        //Insere no input e faz update dele:
        $("#datasCalendario").val(data_nova);
    })
</script>