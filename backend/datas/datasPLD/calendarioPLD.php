<input type="text" id="datasCalendarioPLD" class="custom-select dropdown-menu-right dropdown2 dropdown" style="text-align: center;" onChange="updateCalendarioPLD()" />
<script type="text/javascript">
    var dPld = new Date();
    var diaPld = dPld.getDate();
    var mesPld = dPld.getMonth() + 1;
    var anoPld = dPld.getFullYear();
    var dataCompletaPld = "<?php echo date('d-m-Y'); ?>";

    function updateCalendarioPLD() {
        //Configurando tempo de vida da cookie ao executar funçao:
        var tempoCookie = new Date();
        tempoCookie.setTime(tempoCookie.getTime() + 1 * 3600 * 1000);

        //Definindo valores da funçao:
        var selectCalendarioPld = document.getElementById('datasCalendarioPLD');
        var recojeCalendarioPld = selectCalendarioPld.value;
        console.log('Data nova de pld:' + recojeCalendarioPld);
        document.cookie = "data_pld = " + recojeCalendarioPld + "; expires=" + tempoCookie.toUTCString();
        var anoNovoPld = recojeCalendarioPld.substring(6, 10);
        var mesNovoPld = recojeCalendarioPld.substring(5, 3);
        var totalDiasPld = new Date();
        var ultimoFormateadoPld = recojeCalendarioPld.substring(0, 2);
        var finalNovaDataPld = anoNovoPld + '-' + mesNovoPld + '-' + ultimoFormateadoPld;
        console.log('Final da data de pld:' + finalNovaDataPld);
    }

    $('#datasCalendarioPLD').datepicker({
        format: "dd-mm-yyyy",
        language: "pt-BR",
        zIndexOffset: 10000,
    }).datepicker('update', dataCompletaPld);
</script>