<?php
class MessagesView
{
    // Function que pega o valor validado de erro e tenta ver que mensagem de erro pode oferecer.
    public function showMsg($status, $msg)
    {
        switch ($status) {
            case 200:
                //return array("status" => 200, "msg" => "Executado com sucesso");
                return array("status" => 200, "msg" => $msg);
                break;

            case 400:
                //return array("status" => 400, "msg" => "Bad request");
                return array("status" => 400, "msg" => $msg);
                break;

            case 406:
                //return array("status" => 406, "msg" => "");
                return array("status" => 406, "msg" => $msg);
                break;

            default:
                //return array("status" => "", "msg" => "Nada a mostrar");
                return array("status" => $status, "msg" => $msg);
                break;
        }
    }
}
