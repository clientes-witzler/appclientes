<?php

class ListaDatas
{
    // Atributos
    public $id;
    public $cookie;
    public $formatacao;
    public $view;
    public $data;

    //Métodos especiais:
    public function __construct($id, $formatacao, $view, $cookie, $data)
    {
        $this->setId($id);
        $this->setCookie($cookie);
        $this->setFormatacao($formatacao);
        $this->setView($view);
        $this->setData($data);
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of cookie
     */
    public function getCookie()
    {
        return $this->cookie;
    }

    /**
     * Set the value of cookie
     *
     * @return  self
     */
    public function setCookie($cookie)
    {
        $this->cookie = $cookie;

        return $this;
    }

    /**
     * Get the value of formatacao
     */
    public function getFormatacao()
    {
        return $this->formatacao;
    }

    /**
     * Set the value of formatacao
     *
     * @return  self
     */
    public function setFormatacao($formatacao)
    {
        if ($formatacao == "dd-mm-yyyy" || $formatacao == "mm-yyyy" || $formatacao == "yyyy") {
            $this->formatacao = $formatacao;
        } else {
            $this->formatacao = false;
        }
        return $this;
    }

    /**
     * Get the value of view
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set the value of view
     *
     * @return  self
     */
    public function setView($view)
    {
        if ($view == "days" || $view == "months" || $view == "years" || $view == "decades" || $view == "centuries" || $view == "millenium") {
            $this->view = $view;
        } else {
            $this->view = false;
        }
        return $this;
    }

    /**
     * Get the value of data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     *
     * @return  self
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    // Métodos publicos e protegidos:
    public function criarVisual()
    {
        if ($this->getFormatacao() && $this->getView()) {
            $id = $this->getId();
            $cookie = $this->getCookie();
            $formatacao = $this->getFormatacao();
            $view = $this->getView();
            $data = $this->getData();
            echo "<input type=\"text\" id=\"$id\" class=\"custom-select dropdown-menu-right dropdown2 dropdown\" onChange=\"mudarCalendario('$id', '$cookie')\"/>";
            echo "<script>configCalendar('$id', '$formatacao', '$view', '$data');</script>";
        } else {
            echo "<script>console.log('A classe não foi bem definida para este calendario');</script>";
        }
    }
}
