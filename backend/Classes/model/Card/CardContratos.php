<?php
class CardContratos extends Montantes
{
    // Atributos:
    protected $idUnidadeUrl;
    protected $anoBusqueda;
    protected $indicesAnoAtual;

    // Métodos especiais:
    public function __construct($anoBusqueda, $idUnidadeUrl = 0)
    {
        $this->definirUsuario();
        $this->setIdUnidadeUrl($idUnidadeUrl);
        $this->setAnoBusqueda($anoBusqueda);
        $this->setUrlApi("contrato2?id_unidades=" . $this->getIdUnidadeUrl());
        $this->contratosApi();
        $this->comercializadoraApi();
    }

    /**
     * Get the value of idUnidadeUrl
     */
    public function getIdUnidadeUrl()
    {
        return $this->idUnidadeUrl;
    }

    /**
     * Set the value of idUnidadeUrl
     *
     * @return  self
     */
    public function setIdUnidadeUrl($idUnidadeUrl)
    {
        $this->idUnidadeUrl = $idUnidadeUrl;

        return $this;
    }

    /**
     * Get the value of anoBusqueda
     */
    public function getAnoBusqueda()
    {
        return $this->anoBusqueda;
    }

    /**
     * Set the value of anoBusqueda
     *
     * @return  self
     */
    public function setAnoBusqueda($anoBusqueda)
    {
        $this->anoBusqueda = $anoBusqueda;

        return $this;
    }

    /**
     * Get the value of indicesAnoAtual
     */
    public function getIndicesAnoAtual()
    {
        return $this->indicesAnoAtual;
    }

    /**
     * Set the value of indicesAnoAtual
     *
     * @return  self
     */
    public function setIndicesAnoAtual($indicesAnoAtual)
    {
        $this->indicesAnoAtual = $indicesAnoAtual;

        return $this;
    }

    // Métodos publicos:
    public function contratosApi()
    {
        //Arrays utilizados:
        $u = 0;
        $arrId = [];
        $arrIdDistribuidora = [];
        $arrAno = [];
        $arrVValues = [];
        $arrNContract = [];
        $arrPreco = [];
        $arrFlexMax = [];
        $arrFlexMin = [];
        $arrSazoMax = [];
        $arrSazoMin = [];
        //Dados api
        $url = $this->getUrlApi();
        @$recolhe = file_get_contents($url, false, $this->getContextToken());
        @$result = json_decode($recolhe);
        if ($result) {
            foreach ($result as $contratos) {
                $arrV[$u] = [];

                $id = $contratos->id;
                $id_distribuidora = $contratos->id_distribuidora;
                $ano = $contratos->ano;
                $n_contract = $contratos->n_contract;
                $preco = $contratos->preco;
                $flex_max = $contratos->flex_max;
                $flex_min = $contratos->flex_min;
                $sazo_max = $contratos->sazo_max;
                $sazo_min = $contratos->sazo_min;
                $v1 = $contratos->v1;
                $v2 = $contratos->v2;
                $v3 = $contratos->v3;
                $v4 = $contratos->v4;
                $v5 = $contratos->v5;
                $v6 = $contratos->v6;
                $v7 = $contratos->v7;
                $v8 = $contratos->v8;
                $v9 = $contratos->v9;
                $v10 = $contratos->v10;
                $v11 = $contratos->v11;
                $v12 = $contratos->v12;

                array_push($arrId, $id);
                array_push($arrIdDistribuidora, $id_distribuidora);
                array_push($arrAno, $ano);
                array_push($arrNContract, $n_contract);
                array_push($arrPreco, $preco);
                array_push($arrFlexMax, $flex_max);
                array_push($arrFlexMin, $flex_min);
                array_push($arrSazoMax, $sazo_max);
                array_push($arrSazoMin, $sazo_min);
                array_push($arrV[$u], $v1);
                array_push($arrV[$u], $v2);
                array_push($arrV[$u], $v3);
                array_push($arrV[$u], $v4);
                array_push($arrV[$u], $v5);
                array_push($arrV[$u], $v6);
                array_push($arrV[$u], $v7);
                array_push($arrV[$u], $v8);
                array_push($arrV[$u], $v9);
                array_push($arrV[$u], $v10);
                array_push($arrV[$u], $v11);
                array_push($arrV[$u], $v12);
                array_push($arrVValues, $arrV[$u]);
                $u++;
            }
            $newArray = array("id" => $arrId, "idDistribuidora" => $arrIdDistribuidora, "ano" => $arrAno, "nContract" => $arrNContract, "preco" => $arrPreco, "flexMax" => $arrFlexMax, "flexMin" => $arrFlexMin, "sazoMax" => $arrSazoMax, "sazoMin" => $arrSazoMin, "arrV" => $arrVValues);
            $this->setValoresGrafico($newArray);
            $this->setIndicesAnoAtual($this->separarAno());
        }
    }

    public function comercializadoraApi()
    {
        if ($this->separarAno()) {
            $values = $this->getValoresGrafico();
            $indices = $this->getIndicesAnoAtual();
            $totalComercializadora = count($indices);
            if ($totalComercializadora > 0) {
                $arrNomeComercializadora = [];
                $arrPath = [];
                for ($i = 0; $i < $totalComercializadora; $i++) {
                    $id_comercializadora_atual = $values["idDistribuidora"][$indices[$i]];
                    $urlComercializadora = "https://api.develop.clientes.witzler.com.br/api/comercializadora?id_comercializadora=" . $id_comercializadora_atual;
                    @$recolheComercializadora = file_get_contents($urlComercializadora, false, $this->getContextToken());
                    @$resultComercializadora = json_decode($recolheComercializadora);
                    foreach ($resultComercializadora as $comercializadora) {
                        $nome_comercializadora = $comercializadora->nome;
                        $path_comercializadora = $comercializadora->path;

                        array_push($arrNomeComercializadora, $nome_comercializadora);
                        array_push($arrPath, $path_comercializadora);
                    }
                }
                $newArray = array("nomeComercializadora" => $arrNomeComercializadora, "path" => $arrPath);
                return $newArray;
            }
        }
    }

    public function gerarCard($id = 0, $display = "none")
    {
        $values = $this->getValoresGrafico();
        $indices = $this->getIndicesAnoAtual();
        $comercializadora = $this->comercializadoraApi();
        $u = $indices[$id];
        // Separa valores utilizados:
        $ano = $values["ano"][$u];
        $path = $comercializadora["path"][$id];
        $nome = $comercializadora["nomeComercializadora"][$id];
        $nContract = $values["nContract"][$u];
        $preco = round($values["preco"][$u], 2);
        $flexMin = $values["flexMin"][$u];
        $flexMax = $values["flexMax"][$u];
        $sazoMin = $values["sazoMin"][$u];
        $sazoMax = $values["sazoMax"][$u];
        // Valores do gráfico:
        $idGrafico = "chartDoughnutContrato" . $id;
        $varChartGrafico = "charMontantes" . $id;
        $idUnidade =  $this->getIdUnidadeUrl();

        echo "
            <div class=\"col-12 col-sm-6 col-md-6 col-lg-4 fade-in\" id=\"contrato$id\" style=\"display: $display;\">
                <div class=\"card-accent-secondary card\">
                    <div class=\"text-center h-25 card-header\">
                        <div class=\"row\">
                            <div class=\"col\">
                                <div class=\"mb-0 pt-3 card-title\">
                                    <img height=\"50\" src=\"https://clientes.witzler.com.br/backend/assets/img/distributor/$path\">
                                    <hr>
                                    <h5 class=\"mt-3\">Janeiro/$ano A Dezembro/$ano</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"card-body\">
                        <div class=\"table-responsive\">
                            <table class=\"table\">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class=\"col-12 container row noPadding noMargin\">
                                                <div class=\"col-6 noPadding text-left\">
                                                    Comercializadora:
                                                </div>
                                                <div class=\"col-6 noPadding text-right\">
                                                    $nome
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class=\"col-12 container row noPadding noMargin\">
                                                <div class=\"col-6 noPadding text-left\">
                                                    Número do contrato:
                                                </div>
                                                <div class=\"col-6 noPadding text-right\">
                                                    $nContract
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class=\"col-12 container row noPadding noMargin\">
                                                <div class=\"col-6 noPadding text-left\">
                                                    Preço Contratado [R$/MWh]:
                                                </div>
                                                <div class=\"col-6 noPadding text-right\">
                                                    $preco
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class=\"col-12 container row noPadding noMargin\">
                                                <div class=\"col-6 noPadding text-left\">
                                                    Flexibilidade:
                                                </div>
                                                <div class=\"col-6 noPadding text-right\">
                                                    $flexMin / $flexMax
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class=\"col-12 container row noPadding noMargin\">
                                                <div class=\"col-6 noPadding text-left\">
                                                    Sazonalidade:
                                                </div>
                                                <div class=\"col-6 noPadding text-right\">
                                                    $sazoMin / $sazoMax
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style=\"height: 250px; \">

                                                <div class=\"chartWrapper\" style=\"position: relative;\">
                                                    <div class=\"chartAreaWrapper\" id=\"graficoContrato$id\" style=\"overflow-x: auto;\">
                                                        <canvas id=\"chartDoughnutContrato$id\" height=\"250\" width=\"400\" class=\"mx-auto\" style=\"display: block;\"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        ";
        // Parte do grafico:
        echo "<script>";
        $this->setIdGrafico($idGrafico);
        $this->varChartData($varChartGrafico, array("Flex máx", "Valor médio", "Flex min"), array(), array("#c8c8c8", "#E98E29", "#c8c8c8"), array("#c8c8c8", "#E98E29", "#c8c8c8"));
        $this->configChartData($varChartGrafico, "", "Montante");
        echo "</script>";
    }
}
