<?php
class FatorPotencia extends Grafico implements GerarGraficoInterface
{
    // Atributos:
    protected $dataGerada;
    protected $idUnidadeAtiva;

    // Metodos especiais:
    public function __construct($idGrafico = "", $dataGerada, $idUnidadeAtiva, $parametrosApi = array("capacitivo" => array(), "hora_do_dia" => array()))
    {
        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setDataGerada($dataGerada);
        $this->setIdUnidadeAtiva($idUnidadeAtiva);
        $this->setUrlApi("periodo/fator/potencia/hora?data_busca=" . $this->getDataGerada() . "&unidade_id=" . $this->getIdUnidadeAtiva());
        $this->setParametrosApi($parametrosApi);
        $this->recolherDados();                     // Define valoresGrafico
    }

    /**
     * Get the value of dataGerada
     */
    public function getDataGerada()
    {
        return $this->dataGerada;
    }

    /**
     * Set the value of dataGerada
     *
     * @return  self
     */
    public function setDataGerada($dataGerada)
    {
        $dataGerada = date("Y-m-d", strtotime($dataGerada));
        $this->dataGerada = $dataGerada;

        return $this;
    }

    /**
     * Get the value of idUnidadeAtiva
     */
    public function getIdUnidadeAtiva()
    {
        return $this->idUnidadeAtiva;
    }

    /**
     * Set the value of idUnidadeAtiva
     *
     * @return  self
     */
    public function setIdUnidadeAtiva($idUnidadeAtiva)
    {
        $this->idUnidadeAtiva = $idUnidadeAtiva;

        return $this;
    }

    // Metodos publicos:
    public function updateDados()
    {
        if ($this->getValoresGrafico()) {
            $values = $this->getValoresGrafico();
            $arrCapacitivo = array();
            $arrHora = array();
            for ($i = 0; $i < count($values["capacitivo"]); $i++) {
                //Formatando a hora:
                $unidadeHora = $values["hora_do_dia"][$i] - 1;
                if ($unidadeHora < 10) {
                    $unidadeHora = "0$unidadeHora";
                }
                $unidadeHora = $unidadeHora . ":00";
                //Formatando Unidade:
                $unidadeCapacitivo = number_format($values["capacitivo"][$i], 2);

                array_push($arrHora, $unidadeHora);
                array_push($arrCapacitivo, $unidadeCapacitivo);
            }
            $newValues = array("capacitivo" => $arrCapacitivo, "hora_do_dia" => $arrHora);
        } else {
            $newValues = array("capacitivo" => array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), "hora_do_dia" => array("00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"));
        }
        return $newValues;
    }

    public function varChartData($data, $labelsName = array(), $labelsId = array(), $backgroundColor = array(), $borderColor = array())
    {
        $values = $this->getValoresGrafico();
        echo "
        var $data = {
            labels: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
            datasets: [
                {
                    type: 'bar',
                    label: '$labelsName[0]',
                    backgroundColor: '$backgroundColor[0]',
                    data: [
        ";
        for ($i = 0; $i < 24; $i++) {
            $valor = isset($values["capacitivo"][$i]) ? $values["capacitivo"][$i] : 0;
            echo $valor . ", ";
        }
        echo "
                    ],
                },
                {
                    type: 'line',
                    label: '$labelsName[1]',
                    data: [0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92],
                    fill: false,
                    borderWidth: 1,
                    borderColor: '$borderColor[0]',
                    borderDash: [5, 4],
                    lineTension: 0,
                    steppedLine: true
                },
                {
                    type: 'line',
                    label: '$labelsName[2]',
                    data: [-0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92],
                    fill: false,
                    borderWidth: 1,
                    borderColor: '$borderColor[1]',
                    borderDash: [5, 4],
                    lineTension: 0,
                    steppedLine: true
                }
            ]
        };
        ";
    }

    public function configChartData($data, $tipo = "", $titleText)
    {
        if ($this->getIdGrafico()) {
            $id = $this->getIdGrafico();
            echo "
            var ctx = document.getElementById('$id').getContext('2d');
			window.graficoFatorPotencia = new Chart(ctx, {
				type: '$tipo',
				data: $data,
				options: {
                    responsive: false,
                    maintainAspectRatio: true,
                    legend: {
                        fullWidth: false,
                        position: 'bottom',
                        labels: {
                            padding: 2
                        },
                    },
                    title: {
                        display: false,
                        text: '$titleText'
                    },
                    tooltips: {
						mode: 'index',
						intersect: false,
                        callbacks: {
                            label: function(tooltipItem, data) {
                                if(tooltipItem.datasetIndex == 0) {
                                    var fatorPotencia = data.datasets[0].data[tooltipItem.index];
                                    return \"Fator de potência: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(fatorPotencia);
                                } else if(tooltipItem.datasetIndex == 1) {
                                    var limIndutivo = data.datasets[1].data[tooltipItem.index];
                                    return \"Limite indutivo: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(limIndutivo);
                                } else if(tooltipItem.datasetIndex == 2) {
                                    var limCapacitivo = data.datasets[2].data[tooltipItem.index];
                                    return \"Limite capacitivo: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(limCapacitivo);
                                }
                            },
                        }
					},
                    scales: {
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true
                            },
                            stacked: false,
                            ticks: {
                                min: 0,
                                stepSize: 0.5,
                                callback: function(label, index, labels){
                                    return formatadorDeNumeroInglesParaNumeroBrasileiro(label);
                                }   
                            }
                        }],
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true
                            },
                            stacked: false,
                        }],
                    },
                    animation: {
                        duration: 4000,
                    },
                    chartArea: {
                        width: '100%',
                        height: '100%',
                        padding: {
                            top: 0,
                            bottom: 0,
                        },
                    },
                }
			});
            ";
        }
    }
}
