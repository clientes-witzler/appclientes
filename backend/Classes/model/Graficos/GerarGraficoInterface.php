<?php
interface GerarGraficoInterface {
    public function varChartData($data, $labelsName = array(), $labelsId = array(), $backgroundColor = array(), $borderColor = array());
    public function configChartData($data, $tipo = "", $titleText);
}