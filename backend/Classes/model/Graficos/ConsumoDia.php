<?php
class ConsumoDia extends Grafico implements GerarGraficoInterface
{
    // Atributos:
    protected $dataGrafico;
    protected $idUnidadeGrafico;

    // Metodos especiais:
    public function __construct($idGrafico, $dataGrafico, $idUnidadeGrafico)
    {
        $parametrosApi = array("ativo_c" => array());

        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setDataGrafico($dataGrafico);
        $this->setIdUnidadeGrafico($idUnidadeGrafico);
        $this->setParametrosApi($parametrosApi);
        $this->setUrlApi("periodo/ativoc?data_busca=" . $this->getDataGrafico() . "&unidade_id=" . $this->getIdUnidadeGrafico());
        $this->recolherDados();                     // Define valoresGrafico
    }

    /**
     * Get the value of dataGrafico
     */
    public function getDataGrafico()
    {
        return $this->dataGrafico;
    }

    /**
     * Set the value of dataGrafico
     *
     * @return  self
     */
    public function setDataGrafico($dataGrafico)
    {
        $dataGrafico = date("Y-m-d", strtotime($dataGrafico));
        $this->dataGrafico = $dataGrafico;

        return $this;
    }

    /**
     * Get the value of idUnidadeGrafico
     */
    public function getIdUnidadeGrafico()
    {
        return $this->idUnidadeGrafico;
    }

    /**
     * Set the value of idUnidadeGrafico
     *
     * @return  self
     */
    public function setIdUnidadeGrafico($idUnidadeGrafico)
    {
        $this->idUnidadeGrafico = $idUnidadeGrafico;

        return $this;
    }

    // Metodos publicos:
    public function varChartData($data, $labelsName = array(), $labelsId = array(), $backgroundColor = array(), $borderColor = array())
    {
        $values = $this->getValoresGrafico();
        echo "
        var $data = {
            labels: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
            datasets: [
        ";
        for ($i = 0; $i < count($labelsName); $i++) {
            echo "
                {
                    label: '$labelsName[$i]',
                    backgroundColor: '$backgroundColor[$i]',
                    borderColor: '$borderColor[$i]',
                    pointRadius: 0,
                    fill: true,
                    data: [
            ";
            for ($j = 0; $j < 24; $j++) {
                $valor = isset($values[$labelsId[$i]][$j]) ? $values[$labelsId[$i]][$j] : 0;
                echo "$valor, ";
            }
            echo "
                    ]
                },
        ";
        }
        echo "
            ]
        };
        ";
    }

    public function configChartData($data, $tipo = "", $titleText)
    {
        if ($this->getIdGrafico()) {
            $id = $this->getIdGrafico();
            echo "
            var ctx = document.getElementById('$id').getContext('2d');
			window.graficoDia = new Chart(ctx, {
				type: '$tipo',
				data: $data,
				options: {
					responsive: false,
					maintainAspectRatio: true,
					title: {
						display: false,
						text: '$titleText'
					},
					tooltips: {
						mode: 'index',
						intersect: false,
                        callbacks: {
                            label: function(tooltipItem, data) {
                                var labelText = data.labels[tooltipItem.index];
                                var valueData = data.datasets[0].data[tooltipItem.index];
                                return \"Consumo geral(KWh): \" + formatadorDeNumeroInglesParaNumeroBrasileiro(valueData);
                                console.log(data);
                            },
                        }
					},
					scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Hora'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                            },
                            ticks:{
                                callback: function(label, index, labels){
                                    return formatadorDeNumeroInglesParaNumeroBrasileiro(label);
                                }    
                            }
                        }]
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true,
                        duration: 4000,
                    }
				}
			});
            ";
        }
    }
}
