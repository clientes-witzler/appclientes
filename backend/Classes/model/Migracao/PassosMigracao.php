<?php
class PassosMigracao extends Usuario
{
    // Atributos:
    public $idEtapa;
    public $etapa;
    public $titulo;
    public $descricao;

    // Metodos especiais:
    public function __construct()
    {
        $this->definirUsuario();
    }

    /**
     * Get the value of idEtapa
     */
    public function getIdEtapa()
    {
        return $this->idEtapa;
    }

    /**
     * Set the value of idEtapa
     *
     * @return  self
     */
    public function setIdEtapa($idEtapa)
    {
        $this->idEtapa = $idEtapa;

        return $this;
    }

    /**
     * Get the value of etapa
     */
    public function getEtapa()
    {
        return $this->etapa;
    }

    /**
     * Set the value of etapa
     *
     * @return  self
     */
    public function setEtapa($etapa)
    {
        $this->etapa = $etapa;

        return $this;
    }

    /**
     * Get the value of titulo
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set the value of titulo
     *
     * @return  self
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get the value of descricao
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set the value of descricao
     *
     * @return  self
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    // Metodos publicos:
    public function gerarPasso($id, $etapa, $titulo, $descricao)
    {
        $this->setIdEtapa($id);
        $this->setEtapa($etapa);
        $this->setTitulo($titulo);
        $this->setDescricao($descricao);

        echo "
        <li class=\"event\" data-date=\"" . $this->getEtapa() . "\">
            <h3 class=\"tituloM" . $this->getIdEtapa() . "\"><span class=\"letters\">" . $this->getTitulo() . "</span></h3>
            <p class=\"textoM" . $this->getIdEtapa() . "\"><span class=\"letters\">" . $this->getDescricao() . "</span></p>
        </li>

        <script>
        var textWrapper = document.querySelector('.event .tituloM" . $this->getIdEtapa() . " .letters');
        textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, \"<span class='letter'>$&</span>\");
    
        anime.timeline({
                loop: false
            })
            .add({
                targets: '.event .tituloM" . $this->getIdEtapa() . "',
                scaleY: [0, 1],
                opacity: [0.5, 1],
                easing: \"easeOutExpo\",
                duration: 700
            })
            .add({
                targets: '.event .tituloM" . $this->getIdEtapa() . "',
                easing: \"easeOutExpo\",
                duration: 700,
                delay: 0
            }).add({
                targets: '.event .tituloM" . $this->getIdEtapa() . " .letter',
                opacity: [0, 1],
                easing: \"easeOutExpo\",
                duration: 600,
                offset: '-=775',
                delay: (el, i) => 34 * (i + 1)
            }).add({
                targets: '.event .tituloM" . $this->getIdEtapa() . "',
                //opacity: 0,
                duration: 1000,
                easing: \"easeOutExpo\",
                delay: 1000
            }).add({
                targets: '.event .tituloM" . $this->getIdEtapa() . "',
                //opacity: 0,
                duration: 1000,
                easing: \"easeOutExpo\",
                delay: 1000
            });
        var textWrapper = document.querySelector('.event .textoM" . $this->getIdEtapa() . " .letters');
        textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, \"<span class='letter'>$&</span>\");
    
        anime.timeline({
                loop: false
            })
            .add({
                targets: '.event .textoM" . $this->getIdEtapa() . "',
                scaleY: [0, 1],
                opacity: [0.5, 1],
                easing: \"easeOutExpo\",
                duration: 700
            })
            .add({
                targets: '.event .textoM" . $this->getIdEtapa() . "',
                easing: \"easeOutExpo\",
                duration: 700,
                delay: 0
            }).add({
                targets: '.event .textoM" . $this->getIdEtapa() . " .letter',
                opacity: [0, 1],
                easing: \"easeOutExpo\",
                duration: 600,
                offset: '-=775',
                delay: (el, i) => 34 * (i + 1)
            }).add({
                targets: '.event .textoM" . $this->getIdEtapa() . "',
                //opacity: 0,
                duration: 1000,
                easing: \"easeOutExpo\",
                delay: 1000
            }).add({
                targets: '.event .textoM" . $this->getIdEtapa() . "',
                //opacity: 0,
                duration: 1000,
                easing: \"easeOutExpo\",
                delay: 1000
            });
        </script>
        ";
    }
}
