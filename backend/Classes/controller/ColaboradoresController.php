<?php
class ColaboradoresController
{
    // Request get para recolher dados de listaClientesUsuarioApi
    public function listaClientesUsuario($id_colaborador,$urlRaiz)
    {
        // Inicializa as classes necessarias
        $view = new ColaboradoresView();
        $model = new Colaboradores();

        $model->constructListaClientesUsuario($id_colaborador);
        $values = $model->listaClientesUsuarioApi($urlRaiz);
        return $view->valuesListaClientesUsuario($values);
    }

    // Request get para recolher informacoes de infoUsuarioColaboradorApi
    public function infoUsuarioColaborador($username,$urlRaiz)
    {
        // Inicializa as classes necessarias
        $view = new ColaboradoresView();
        $model = new Colaboradores();

        $model->constructInfoUsuarioColaborador($username);
        $values = $model->infoUsuarioColaboradorApi($urlRaiz);
        $valuesArray = $view->valuesInfoUsuarioColaborador($values);
        $info = isset($values) && !empty($values) ? $valuesArray[0] : array();
        
        return $info;
    }

    // Request get para recolher dados de listaClientesAdminApi
    public function listaClientesAdmin($urlRaiz) {
        // Inicializa as classes necessarias
        $view = new ColaboradoresView();
        $model = new Colaboradores();

        $values = $model->listaClientesAdminApi($urlRaiz);
        return $view->valuesListaClientesUsuario($values);
    }
}
