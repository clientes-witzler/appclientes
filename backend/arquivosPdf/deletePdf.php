<?php
/*
<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
*/
//Aqui se encontra toda a configuração que vai descriptografar e deleta o pdf 3 minutos depois de ser executado.
session_start();
if(isset($_GET['src'])){
    //Formatando datas:
    //$data_unidade_old = $_GET['data_unidade'];
    $data_unidade_old = htmlspecialchars($_GET['data_unidade'], ENT_QUOTES, 'UTF-8');
    $data_unidade = date('Y-m-d', strtotime($data_unidade_old));
    // $tipo_arquivo = $_GET['tipo_arquivo'];
    $tipo_arquivo = htmlspecialchars($_GET['tipo_arquivo'], ENT_QUOTES, 'UTF-8');
    // $id_unidade = $_GET['id_unidades'];
    $id_unidade = htmlspecialchars($_GET['id_unidades'], ENT_QUOTES, 'UTF-8');
    //$prefixSrc = "/opt/lampp/htdocs/web-clientes/backend/assets/files/clientes/";         // LOCALHOST
    $prefixSrc = "/var/www/webWitzler1.0/backend/assets/files/clientes/";                   // SERVIDOR
    $src_sanitizado = htmlspecialchars($_GET['src'], ENT_QUOTES, 'UTF-8');
    $src = $prefixSrc . $src_sanitizado;
    $srcPdf = $src_sanitizado;
    
    // Change pdfs:
    $pdfvalue = substr($srcPdf, 0, -4) . ".decrypt.pdf";

    // Como o arquivo existe, então deleta ele após 3 minutos.
    // URL api delete:
    //$urlDeleteDecrypt = "http://localhost:8080/api/deletar-pdf?arquivo=" . $prefixSrc . $pdfvalue;
    $urlDeleteDecrypt = $_SESSION['conexao']."/api/deletar-pdf?arquivo=" . $prefixSrc . $pdfvalue;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheDeleteDecrypt = file_get_contents($urlDeleteDecrypt, false, $context);
    @$resultDeleteDecrypt = $recolheDeleteDecrypt;

    echo $resultDeleteDecrypt;
}