<?php
/*
<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
*/
//Aqui se encontra toda a configuração que vai descriptografar e deleta o pdf 3 minutos depois de ser executado.
session_start();
if(isset($_GET['src'])){    
    //$prefixSrc = "/opt/lampp/htdocs/web-clientes/backend/assets/files/clientes/";  // LOCALHOST
    $prefixSrc = "/var/www/webWitzler1.0/backend/assets/files/clientes/"; // SERVIDOR
    $src_sanitizado = htmlspecialchars($_GET['src'], ENT_QUOTES, 'UTF-8');
    $src = $prefixSrc . $src_sanitizado;
    $srcPdf = $src_sanitizado;
    
    //URL API:
    $urlDecryptPdf = $_SESSION['conexao']."/api/decripta-pdf?arquivo=" . $src;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheDecryptPdf = file_get_contents($urlDecryptPdf, false, $context);
    @$resultDecryptPdf = $recolheDecryptPdf;
    if(isset($resultDecryptPdf) && !empty($resultDecryptPdf)){
        // Change pdfs:
        //$pdfvalue = str_replace(".pdf", '.decrypt.pdf', $srcPdf);
        $pdfvalue = substr($srcPdf, 0, -4) . ".decrypt.pdf";

        // echo da rota onde está o arquivo
        //echo "http://localhost/web-clientes/backend/assets/files/clientes/" . $pdfvalue;
        echo "https://clientes.witzler.com.br/backend/assets/files/clientes/" . $pdfvalue;
        //echo "https://clientes.witzler.com.br/backend/assets/files/clientes/" . $srcPdf;
    } else {
        //echo "$urlDecryptPdf";
        echo "";
        //echo "https://clientes.witzler.com.br/backend/assets/files/clientes/" . $srcPdf;
    }
}
