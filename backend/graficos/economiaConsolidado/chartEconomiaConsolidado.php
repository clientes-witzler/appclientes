<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
	//Grafico Histórico de Economia Consolidado: ACL X ACR:
	var barChartData2 = {
		labels: [
			<?php
			$limiteDataMedicaoEconomiaConsolidado = count($data_referencia_economia_consolidado);
			for ($z = 0; $z < $limiteDataMedicaoEconomiaConsolidado; $z++) {
				if ($data_referencia_economia_consolidado[$z] != "") {
					echo "'" . $data_referencia_economia_consolidado[$z] . "', ";
				}
			}

			?>
		],
		datasets: [{
				label: 'Economia [R$]',
				//backgroundColor: "rgba(135, 187, 45, 0.7)",
				backgroundColor: "#34aee4",
				borderColor: "#34aee4",
				data: [
					<?php
					$somaCumulativaEconomizado = 0;
					$arrReverso = [];
					$limiteValorEconomizado = count($valor_economizado);
					for ($n = 0; $n < $limiteValorEconomizado; $n++) {
						$somaCumulativaEconomizado += $valor_economizado[$n];
						array_push($arrReverso, $somaCumulativaEconomizado);
						//echo $somaCumulativaEconomizado . ', ';
					}
					$valorMaxReverso = count($arrReverso) - 1;
					for($m = $valorMaxReverso; $m >= 0; $m--){
						echo $arrReverso[$m].', ';
					}
					?>
				],
				borderWidth: 3,
				type: 'line',
				fill: false,
				yAxisID: 'B'
			},
			/*{
				label: 'Soma acumulativa Economia [R$]',
				//backgroundColor: "rgba(135, 187, 45, 0.7)",
				backgroundColor: "rgba(135, 187, 45, 0.5)",
				data: [
					<?php
					
					$limiteValorEconomizado = count($valor_economizado);
					for ($n = 0; $n < $limiteValorEconomizado; $n++) {
						echo $valor_economizado[$n] . ', ';
					}
					?>
				],
				type: 'bar',
				yAxisID: 'A'
			},*/

		]

	};

	var ctx2 = document.getElementById('historico_de_economia_consolidado_acl_x_acr').getContext('2d');
	window.myBar = new Chart(ctx2, {
		type: 'bar',
		//type: 'line',
		data: barChartData2,
		options: {
			responsive: false,
			maintainAspectRatio: true,
			title: {
				display: true,
				text: 'Consolidado: ACL x ACR'
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			//responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [
					/*{
					stacked: true
					}*/
					/*{
						id: 'A',
						position: 'left',
						stacked: false,
						ticks: {
							min: 0,
						},
					},*/
					{
						id: 'B',
						position: 'left',
						type: 'linear',
						ticks: {
							min: 0,
						},
						stacked: false,
						//display: false
					},
				]
			},
			animation: {
				duration: 4000,
			}
		}
	});
</script>