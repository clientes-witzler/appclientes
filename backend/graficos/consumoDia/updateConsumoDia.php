<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
session_start();
require_once "../../Classes/autoload.php";
//Formateia data
$dataDia_old = $_GET['data_consumo_dia'];
$dataDia = date('Y-m-d', strtotime($dataDia_old));
//Pega id unidade
$id_unidade_dia = $_GET['id_unidades'];
$dia = new ConsumoDia("chartConsumoDia", $dataDia, $id_unidade_dia);
?>
<canvas class="mx-auto fade-in" id="chartConsumoDia" width="750" height="350" style="display: block; height: 350px; width: 690px;"></canvas>
<script>
    <?php
    $dia->varChartData("chartCDia", array("Consumo geral (KWh)"), array("ativo_c"), array("#34aee9"), array("#34aee9"));
    $dia->configChartData("chartCDia", "line", "Consumo por Dia");
    ?>
</script>