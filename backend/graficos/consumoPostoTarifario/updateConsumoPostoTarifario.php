<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
session_start();
require_once "../../Classes/autoload.php";
//Formatar datas
$dataInicialPostoTarifario_old = $_GET['data_consumo_posto_tarifario'];
$data = date('Y-m-d', strtotime($dataInicialPostoTarifario_old));
//id unidade:
$idUnidade = $_GET['id_unidades'];
$postoTarifario = new PostoTarifario("chartDoughnutPostoTarifario", $data, $idUnidade);
?>
<canvas class="mx-auto chartjs-render-monitor fade-in" id="chartDoughnutPostoTarifario" width="1107" height="269" style="display: block; height: 300px; width: 1231px;"></canvas>
<script>
    <?php
        //  $postoTarifario->varChartData("chartPostoTarifario", array("Ponta (KWh)", "Fora Ponta (KWh)"), array("ponta", "foraPonta"), array("rgba(76, 142, 173, 0.7)", "rgba(29, 107, 170, 0.7)"));
        $postoTarifario->varChartData("chartPostoTarifario", array("Ponta (KWh)", "Fora Ponta (KWh)"), array("ponta", "foraPonta"), array($_SESSION['cor-custom-2'], $_SESSION['cor-custom-1']));
        $postoTarifario->configChartData("chartPostoTarifario", "doughnut", "Consumo por Posto Tarifário");
    ?>
</script>