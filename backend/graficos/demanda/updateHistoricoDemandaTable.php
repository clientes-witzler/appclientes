<?php
session_start();
require_once "../../Classes/autoload.php";

if (isset($_GET['id_unidades'])) {
    $id_unidades = $_GET['id_unidades'];
    $dataInicial = date("Y-m-01");

    $hdemandastable = new Demanda("tabelaHistoricoDemadas", $dataInicial, $id_unidades);
    $valuesDem = $hdemandastable->obterDados();
?>
    <table class="table table-striped col-12">
        <thead>
            <tr>
                <th scope="col" class="text-center"><b>Mês</b></th>
                <th scope="col" class="text-center"><b>Demanda Contratada Ponta (KW)</b></th>
                <th scope="col" class="text-center"><b>Demanda Contratada Fora Ponta (KW)</b></th>
                <th scope="col" class="text-center"><b>Demanda Registrada Ponta (KW)</b></th>
                <th scope="col" class="text-center"><b>Demanda Registrada Fora Ponta (KW)</b></th>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 12; $i >= 0; $i--) {
                $mes = date("d/m/Y", strtotime($valuesDem["mes_ref"][$i]));
                $v_dcponta = is_numeric($valuesDem["c_demanda_ponta"][$i]) && $valuesDem["c_demanda_ponta"][$i] != 0 ? $valuesDem["c_demanda_ponta"][$i] : "0.00";
                $v_dcfponta = is_numeric($valuesDem["c_demanda_fponta"][$i]) && $valuesDem["c_demanda_fponta"][$i] != 0 ? $valuesDem["c_demanda_fponta"][$i] : "0.00";
                $v_drponta = is_numeric($valuesDem["demanda_ponta"][$i]) && $valuesDem["demanda_ponta"][$i] != 0 ? $valuesDem["demanda_ponta"][$i] : "0.00";
                $v_drfponta = is_numeric($valuesDem["demanda_fponta"][$i]) && $valuesDem["demanda_fponta"][$i] != 0 ? $valuesDem["demanda_fponta"][$i] : "0.00";

                if($v_drponta > 1.05 * $v_dcponta) {
                    $color1 = "red";
                } else {
                    $color1 = "green";
                }

                if($v_drfponta > 1.05 * $v_dcfponta) {
                    $color2 = "red";
                } else {
                    $color2 = "green";
                }

                echo "
                <tr>
                    <th scope=\"row\" class=\"text-center\"><b>$mes</b></th>
                    <td class=\"text-center\"><b>$v_dcponta</b></td>
                    <td class=\"text-center\"><b>$v_dcfponta</b></td>
                    <td class=\"text-center\" style=\"color: $color1\"><b>$v_drponta</b></td>
                    <td class=\"text-center\" style=\"color: $color2\"><b>$v_drfponta</b></td>
                </tr>
                ";
            }
            ?>
        </tbody>
    </table>
<?php
}
?>