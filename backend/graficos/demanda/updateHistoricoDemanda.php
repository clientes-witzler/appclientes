<?php
session_start();
require_once "../../Classes/autoload.php";

if (isset($_GET['id_unidades'])) {
    $id_unidades = $_GET['id_unidades'];
    $dataInicial = date("Y-m-d");
?>
    <canvas id="chartBarHistoricoDemadas" class="fade-in" width="1540" height="380" style="position: relative;"></canvas>
    <?php
    $hdemandas = new Demanda("chartBarHistoricoDemadas", $dataInicial, $id_unidades);
    ?>
    <script>
        <?php
        $hdemandas->varChartData("chartHistoricoDemadas");
        $hdemandas->configChartData("chartHistoricoDemadas", "bar", "Historico de Demandas");
        ?>
    </script>
<?php
}
?>