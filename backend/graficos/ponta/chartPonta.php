<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<?php
//Recolher os dias do mês atual
$finalMesAtual = date("t");
//echo "<script>console.log($finalMesAtual);</script>";
?>

<script>
	//console.log("final do mes atual charponta: <?php echo $finalMesAtual; ?>");
    //GRAFICO Medição Mensal de Consumo

    var barChartData = {
      labels: [
        <?php
		  $diaDataMedicao = 1;
		  for($o = 0; $o < $finalMesAtual; $o++){
			$labelDataMedicao = isset($data_medicao[$o]) ? $data_medicao[$o] : $diaDataMedicao;
			echo "'$labelDataMedicao', ";

			$diaDataMedicao++;
		  }
        ?>
      ],
			datasets: [{
				label: 'Fora Ponta',
				backgroundColor: "rgba(29, 107, 170, 0.7)",
				data: [
					<?php
					for($i = 0; $i < $finalMesAtual; $i++){
						$valorForaPonta = isset($foraPonta[$i]) ? $foraPonta[$i] : 0;
						echo $valorForaPonta.', ';
					}
					?>
				]
			}, {
				label: 'Ponta',
				backgroundColor: "rgba(76, 142, 173, 0.7)",
				data: [
					<?php
						for($j = 0; $j < $finalMesAtual; $j++){
							$valorPonta = isset($ponta[$j]) ? $ponta[$j] : 0;
							echo $valorPonta.', ';
						}
					?>
				]
			}]
		};
			var ctx = document.getElementById('medicao_mensal_de_consumo').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: false,
					maintainAspectRatio: true,
					title: {
						display: false,
						text: 'Gráfico de Medição'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					},
					animation: {
						duration: 4000,
                    }
				}
			});
			
</script>