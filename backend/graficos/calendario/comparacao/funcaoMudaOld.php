<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    var lista = [];
    var datasset = [];
    $("#calendar-body").find("td").on('click', function() {
        if ($(this).hasClass("bg-light")) {
            $(this).removeClass("bg-light");
            var elemento = $(this).attr("id");
            var formata_elemento = elemento.substr(0, 2);
            var indice = lista.indexOf(formata_elemento);
            lista.splice(indice, 1);
            console.log(lista);
            //enviar variavel de listaChart
            datasset.splice(indice, 1);
            console.log(datasset);

            //Pegando datas:
            var calendario = $("#datasCalendario").val();
            var anoNovo = calendario.substring(3, 7);
            var mesNovo = calendario.substring(0, 2);
            var totalDias = new Date();
            var ultimo = new Date(anoNovo, mesNovo, 0);
            var ultimoDia = ultimo.toString("yyyy/mm/dd");
            var ultimoFormateado = ultimoDia.substring(8, 10);
            var finalNovaData = anoNovo + '-' + mesNovo + '-' + ultimoFormateado;

            $("#card-line-chart-all-days").html("");
            $.ajax({
                type: 'POST',
                //url: '../../backend/graficos/calendario/comparacao/graficoComparacao.php',
                url: '../../backend/graficos/calendario/updateComparacao.php',
                data: {
                    'listaItensEscolhidos': lista,
                    'mes': mesNovo,
                    'ano': anoNovo,
                    'dataSet': datasset
                },
                success: function(r) {
                    $("#card-line-chart-all-days").html(r);
                    //alert(r);
                }
            });
        } else {
            $(this).addClass("bg-light");
            var elemento = $(this).attr("id");
            var indice = elemento.substr(0, 2);
            lista.push(indice);
            console.log(lista);
            //console.log(elemento);
            //enviar variavel de listaChart
            datasset.push(listaData[indice - 1]["datasets"][0]["data"]);
            console.log(datasset);


            //Pegando datas:
            var calendario = $("#datasCalendario").val();
            var anoNovo = calendario.substring(3, 7);
            var mesNovo = calendario.substring(0, 2);
            var totalDias = new Date();
            var ultimo = new Date(anoNovo, mesNovo, 0);
            var ultimoDia = ultimo.toString("yyyy/mm/dd");
            var ultimoFormateado = ultimoDia.substring(8, 10);
            var finalNovaData = anoNovo + '-' + mesNovo + '-' + ultimoFormateado;

            $("#card-line-chart-all-days").html("");
            $.ajax({
                type: 'POST',
                //url: '../../backend/graficos/calendario/comparacao/graficoComparacao.php',
                url: '../../backend/graficos/calendario/updateComparacao.php',
                data: {
                    'listaItensEscolhidos': lista,
                    'mes': mesNovo,
                    'ano': anoNovo,
                    'dataSet': datasset
                },
                success: function(r) {
                    $("#card-line-chart-all-days").html(r);
                    //alert(r);
                }
            });
        }

    });
</script>