<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    $("#calendar-body").find("td").on('click', function() {
        if ($(this).hasClass("bg-light")) {
            $(this).removeClass("bg-light");
            var elemento = $(this).attr("id");
            var formata_elemento = elemento.substr(0, 2);
            var indice = formata_elemento;
            ///lista.splice(indice, 1);
            //datasset.splice(indice, 1);
            var select = document.getElementById('idUnidades');
            var textUnidade = select.options[select.selectedIndex].text;

            //Pegando datas:
            var calendario = $("#datasCalendario").val();
            var anoNovo = calendario.substring(3, 7);
            var mesNovo = calendario.substring(0, 2);
            var finalNovaData = textUnidade + "/" + indice + '-' + mesNovo + '-' + anoNovo;
            console.log(finalNovaData);

            // Recolhe dados de dattaset do grafico de comparacao:
            var finalValues = [];
            var finalFechas = [];
            var listadataset = configComparacao.data.datasets;
            // Verifica quantos valores tem:
            for (var i = 0; i < listadataset.length; i++) {
                var arrayAtual = listadataset[i];
                var label = listadataset[i].label;
                var valuesAtual = listadataset[i].data;

                if (label != "Media") {
                    finalValues.push(valuesAtual);
                    finalFechas.push(label);
                }
                // Verifica a posicao do item escolhido e retira da lista
                if (label === finalNovaData) {
                    //finalValues.splice(i, 1);
                    //finalFechas.splice(i, 1);
                    finalValues.pop();
                    finalFechas.pop();
                }
            }


            $("#card-line-chart-all-days").html("");
            $.ajax({
                type: 'POST',
                //url: '../../backend/graficos/calendario/comparacao/graficoComparacao.php',
                url: '../../backend/graficos/calendario/updateComparacao.php',
                data: {
                    'listaItensEscolhidos': finalFechas,
                    'mes': mesNovo,
                    'ano': anoNovo,
                    'dataSet': finalValues
                },
                success: function(r) {
                    $("#card-line-chart-all-days").html(r);
                    //alert(r);
                }
            });
        } else {
            $(this).addClass("bg-light");
            var elemento = $(this).attr("id");
            var indice = elemento.substr(0, 2);
            //lista.push(indice);
            //datasset.push(listaData[indice - 1]["datasets"][0]["data"]);
            var select = document.getElementById('idUnidades');
            var textUnidade = select.options[select.selectedIndex].text;

            //Pegando datas:
            var calendario = $("#datasCalendario").val();
            var anoNovo = calendario.substring(3, 7);
            var mesNovo = calendario.substring(0, 2);
            var finalNovaData = textUnidade + "/" + indice + '-' + mesNovo + '-' + anoNovo;

            // Recolhe dados de dattaset do grafico de comparacao:
            var finalValues = [];
            var finalFechas = [];
            var listadataset = configComparacao.data.datasets;

            for (var i = 0; i < listadataset.length; i++) {
                var arrayAtual = listadataset[i];
                var label = listadataset[i].label;
                var valuesAtual = listadataset[i].data;

                if (label != "Media") {
                    finalValues.push(valuesAtual);
                    finalFechas.push(label);
                }
            }

            // Adiciona os novos valores para o array com os valores recolhidos:
            finalFechas.push(finalNovaData);
            finalValues.push(listaData[indice - 1]["datasets"][0]["data"]);

            $("#card-line-chart-all-days").html("");
            $.ajax({
                type: 'POST',
                //url: '../../backend/graficos/calendario/comparacao/graficoComparacao.php',
                url: '../../backend/graficos/calendario/updateComparacao.php',
                data: {
                    'listaItensEscolhidos': finalFechas,
                    'mes': mesNovo,
                    'ano': anoNovo,
                    'dataSet': finalValues
                },
                success: function(r) {
                    $("#card-line-chart-all-days").html(r);
                    //alert(r);
                }
            });
        }

    });
</script>