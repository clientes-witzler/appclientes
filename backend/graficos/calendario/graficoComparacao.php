<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<canvas id="comparacao" width="1540" height="380" style="display: none; position: relative;"></canvas>
<?php
$ano = isset($_POST['ano']) ? $_POST['ano'] : date('Y');
$mes = isset($_POST['mes']) ? $_POST['mes'] : date('m');
$listaItens = isset($_POST['listaItensEscolhidos']) ? $_POST['listaItensEscolhidos'] : array();
$dataSet = isset($_POST['dataSet']) ? $_POST['dataSet'] : array();

$comparacao = new ComparacaoCalendario("comparacao", $ano, $mes, $listaItens, $dataSet);
echo "<script>";
$comparacao->gerarGrafico();
echo "</script>";
