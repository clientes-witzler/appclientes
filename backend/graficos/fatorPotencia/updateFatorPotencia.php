<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
session_start();
require_once "../../Classes/autoload.php";

//Formatar data
$dataFatorPotencia_old = $_GET['data_fator_potencia'];
$dataFatorPotencia = date('Y-m-d', strtotime($dataFatorPotencia_old));
//id unidade
$idUnidadesFatorPotencia = $_GET['id_unidades'];

$fatorPotencia = new FatorPotencia("chartBarFatorPotencia", $dataFatorPotencia, $idUnidadesFatorPotencia);
?>
<canvas id="chartBarFatorPotencia" class="fade-in" width="1540" height="380" style="display: block; position: relative;"></canvas>
<script>
    <?php
    $fatorPotencia->varChartData("chartFatorPotencia", array("Fator de potencia", "Limite indutivo", "Limite capacitivo"), array("capacitivo"), array("#ace2e2ab"), array("rgba(255, 159, 64, 0.8)", "rgba(255, 159, 64, 0.8)"));
    $fatorPotencia->configChartData("chartFatorPotencia", "bar", "Consumo por Posto Tarifário");
    ?>
</script>