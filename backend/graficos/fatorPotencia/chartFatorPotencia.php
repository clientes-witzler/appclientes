<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    var altura = window.innerHeight;
    var width = window.innerWidth;

    if (width < 700) {
        var tamanho = 10;
        var borda = 10;
    } else if (width > 1200) {
        var tamanho = 12;
        var borda = 40;
    } else if (width > 700 && width < 820) {
        var tamanho = 5;
        var borda = 20;
    } else if (width > 820 && width < 1024) {
        var tamanho = 7.5;
        var borda = 25;
    } else if (width > 1024 && width < 1200) {
        var tamanho = 9;
        var borda = 30;
    }
</script>





<script>
    var barChartData5 = {
        labels: [
            <?php
            $tamanhoListaHora = count($listaHora);
            for ($i = 0; $i < $tamanhoListaHora; $i++) {
                echo "'" . $listaHora[$i] . ":00', ";
            }
            ?>
        ],
        datasets: [{
                type: 'bar',
                label: 'Fator de potencia',
                backgroundColor: "#ace2e2ab",
                data: [
                    <?php
                    $tamanhoListaCapacitivo = count($listaCapacitivo);
                    for ($i = 0; $i < $tamanhoListaCapacitivo; $i++) {
                        echo $listaCapacitivo[$i] . ", ";
                    }
                    ?>
                ],
            },
            {
                type: 'line',
                label: 'Limite indutivo',
                data: [0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92],
                fill: false,
                borderWidth: 1,
                borderColor: 'rgba(255, 159, 64, 0.8)',
                borderDash: [5, 4],
                lineTension: 0,
                steppedLine: true
            },
            {
                type: 'line',
                label: 'Limite capacitivo',
                data: [-0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92],
                fill: false,
                borderWidth: 1,
                borderColor: 'rgba(255, 159, 64, 0.8)',
                borderDash: [5, 4],
                lineTension: 0,
                steppedLine: true
            }
        ]
    };

    var ctx5 = document.getElementById('chartBarFatorPotencia').getContext('2d');
    window.myBar = new Chart(ctx5, {
        type: 'bar',
        data: barChartData5,
        options: {
            responsive: false,
            maintainAspectRatio: true,
            legend: {
                fullWidth: false,
                position: "bottom",

                labels: {
                    /*fontSize: tamanho,*/
                    boxWidth: borda,
                    padding: 2
                },
            },
            title: {
                display: false,
                text: 'Consumo por Posto Tarifário'
            },
            scales: {
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true
                    },
                    stacked: false,
                    ticks: {
                        /*fontSize: tamanho,*/
                        min: 0,
                        stepSize: 0.5
                    }
                }],
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true
                    },
                    stacked: false,
                    ticks: {
                        /*fontSize: tamanho,*/
                    }
                }],
            },
            animation: {
                duration: 4000,
            },
            chartArea: {
                width: '100%',
                height: '100%',
                padding: {
                    top: 0,
                    bottom: 0,
                },
            },
        }
    });
</script>